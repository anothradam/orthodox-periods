var Promise = require('bluebird'),
	dates 	= require('./Dates'),
	Afterfeast = {};

/**
 * Return all afterfeast periods
 *
 * orthodoxDates
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Afterfeast.getAllForYear = args=>new Promise((resolve,reject)=>{
		const {year,calendar} = args;
		var key,
			afterfeasts = {};

		// Get all periods for request date
		dates({year,calendar})
			.then(orthodoxDates=>{

		for(key in orthodoxDates){
			if(key == 'afterfeastPascha'){				afterfeasts['afterfeastPascha']					= orthodoxDates[key];}
			if(key == 'afterfeastPentecost'){			afterfeasts['afterfeastPentecost']				= orthodoxDates[key];}
			if(key == 'afterfeastAscension'){			afterfeasts['afterfeastAscension']				= orthodoxDates[key];}
			if(key == 'afterfeastNativity'){			afterfeasts['afterfeastNativity']				= orthodoxDates[key];}
			if(key == 'afterfeastTheophany'){			afterfeasts['afterfeastTheophany']				= orthodoxDates[key];}
			if(key == 'afterfeastPresentationChrist'){	afterfeasts['afterfeastPresentationChrist']		= orthodoxDates[key];}
			if(key == 'afterfeastTransfiguration'){		afterfeasts['afterfeastTransfiguration']		= orthodoxDates[key];}
			if(key == 'afterfeastDormition'){			afterfeasts['afterfeastDormition']				= orthodoxDates[key];}
			if(key == 'afterfeastNativityTheotokos'){	afterfeasts['afterfeastNativityTheotokos']		= orthodoxDates[key];}
			if(key == 'afterfeastExaltationCross'){		afterfeasts['afterfeastExaltationCross']		= orthodoxDates[key];}
			if(key == 'afterfeastPresentationTheotokos'){afterfeasts['afterfeastPresentationTheotokos']	= orthodoxDates[key];}
		}

		resolve(afterfeasts);
	});
});

/**
 * Export
 * @type {{}}
 */
export default Afterfeast;