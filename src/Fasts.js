'use strict'
var moment  = require('moment'),
    Promise = require('bluebird'),
    dates = require('./Dates'),
    periods = require('./Periods'),
	utils 	= require('./utils'),
	Fasts = {};

/**
 * Returns all fasting periods from the year's periods
 *
 * args.periods
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Fasts.getAll =  args=>new Promise((resolve,reject)=>{
	const calendar = args.calendar;
		args = utils.dateObj(args);
	let key,
		fasts = {};

	// Get all periods for request date
	dates({year:args.year,calendar})
		.then(orthodoxDates=>{

		for (key in orthodoxDates) {
			if (key == 'greatLent') { fasts['greatLent'] = orthodoxDates[key];}
			if (key == 'nativityFast') { fasts['nativityFast'] = orthodoxDates[key];}
			if (key == 'apostlesFast') { fasts['apostlesFast'] = orthodoxDates[key];}
			if (key == 'dormitionFast') {fasts['dormitionFast'] = orthodoxDates[key];}
		}

		resolve(fasts);
	});
});//getFasts


/**
 * Calculates fasting rules for isFastDay method
 *
 * currentPeriods
 * args.dayOfWeek
 *
 * @param args
 * @param is
 * @returns {*}
 */
Fasts.calculateFastingRules = args=>new Promise((resolve,reject)=>{
	const {calendar} = args;
		args = utils.dateObj(args);

	let is = {
		fastingDay  : false,
		wine        : true,
		oil         : true,
		dairy       : true,
		meat        : true,
		fish        : true,
		strict      : false,
		noFood      : false,
		xerophagy   : false // fruit,nut,bread,honey,vegetables boiled in water and salt
	};

	return periods.getPeriodsForDate({date:args.date,calendar})
		.then(currentPeriods=> {
			var max = currentPeriods.length;

			// Determine if it's Wednesday or Friday
			if (args.dayOfWeek == 3) {
				is.fastingDay = true;
				is.wine = false;
				is.oil = false;
				is.fish = false;
				is.dairy = false;
				is.meat = false;
				is.strict = true;
			}

			if (args.dayOfWeek == 5) {
				is.fastingDay = true;
				is.wine = false;
				is.oil = true;
				is.fish = false;
				is.dairy = false;
				is.meat = false;
				is.strict = false;
			}

			// Exit if there are no current periods being passed in
			if (max == 0) { resolve(is);}

			// Fasting rules for each period throughout the year
			for (var i = 0; i < max; i++) {
				switch (currentPeriods[i]) {
					case 'holyWeek' :
						is.fastingDay = true;
						is.wine = false;
						is.oil = false;
						is.dairy = false;
						is.meat = false;
						is.fish = false;
						is.strict = true;
						is.noFood = false;

						//thursday night meal wine & oil allowed
						if (args.dayOfWeek == 4) {
							is.wine = true;
							is.oil = true;
						}

						// Fri, Sat = no food
						if (args.dayOfWeek == 5 || args.dayOfWeek == 6) {
							is.noFood = true;
						}

						// Sat wine & small food (fruit) allowed
						if (args.dayOfWeek == 6) {
							is.noFood = false;
							is.wine = true;
							is.xerophagy = true;
						}

						break;
					case 'firstWeekGreatLent' :
						is.fastingDay = true;
						is.wine = false;
						is.oil = false;
						is.dairy = false;
						is.meat = false;
						is.fish = false;
						is.strict = true;
						is.noFood = true;

						// Food allowed on Wed, Fri
						if (args.dayOfWeek == 3 || args.dayOfWeek == 5) {
							is.noFood = false;
						}

						break;
					case 'greatLent' :

						//@todo Wine and oil are permitted on several feast days if they fall on a weekday during Lent. Consult your parish calendar. On Annunciation and Palm Sunday, fish is also permitted.
						is.fastingDay = true;
						is.wine = false;
						is.oil = false;
						is.dairy = false;
						is.meat = false;
						is.fish = false;
						is.strict = true;
						is.noFood = false;


						//sat, sun wine and oil
						if (args.dayOfWeek == 6 || args.dayOfWeek == 0) {
							is.wine = true;
							is.oil = true;
						}
						break;
					case 'nativityFast' :
						is.fastingDay = true;
						is.wine = false;
						is.oil = false;
						is.fish = false;
						is.dairy = false;
						is.meat = false;

						// Strict fast mon,wed,fri
						if (args.dayOfWeek == 1 || args.dayOfWeek == 3 || args.dayOfWeek == 5) {
							is.strict = true;
						}

						// tues, thursday wine & oil allowed
						if (args.dayOfWeek == 2 || args.dayOfWeek == 4) {
							is.wine = true;
							is.oil = true;
						}

						//sat, sun wine, oil, fish allowed
						if (args.dayOfWeek == 6 || args.dayOfWeek == 0) {
							is.wine = true;
							is.oil = true;
							is.fish = true;
						}
						// @todo Fast gets stricter toward the end of the fast, fish becomes excluded. Add logic for this
						break;
					case 'apostlesFast' :
						is.fastingDay = true;
						is.wine = false;
						is.oil = false;
						is.dairy = false;
						is.meat = false;
						is.fish = false;
						is.strict = false;

						// Strict fast mon,wed,fri
						if (args.dayOfWeek == 1 || args.dayOfWeek == 3 || args.dayOfWeek == 5) {
							is.strict = true;
						}

						// tues, thursday wine & oil allowed
						if (args.dayOfWeek == 2 || args.dayOfWeek == 4) {
							is.wine = true;
							is.oil = true;
						}

						//sat, sun wine, oil, fish allowed
						if (args.dayOfWeek == 6 || args.dayOfWeek == 0) {
							is.wine = true;
							is.oil = true;
							is.fish = true;
						}

						break;
					case 'dormitionFast' :
						is.fastingDay = true;
						is.wine = false;
						is.oil = false;
						is.dairy = false;
						is.meat = false;
						is.fish = false;
						is.strict = true;
						is.noFood = false;

						//sat, sun wine, oil, fish allowed
						if (args.dayOfWeek == 6 || args.dayOfWeek == 0) {
							is.wine = true;
							is.oil = true;
						}

						break;
					case 'cheesefare' :
						is.fastingDay = true;
						is.wine = true;
						is.oil = true;
						is.dairy = true;
						is.meat = false;
						is.fish = true;
						is.strict = false;
						is.noFood = false;

						// Whole week, no meat only
						break;
				}//switch

				if (i + 1 >= max) {

					// Return calculations from above
					resolve(is);
				}//if
			}//for
		});
});

/**
 * Determines if request Date is a fast day
 *  @todo The Eve of Theophany, the Exaltation of the Cross and the Beheading of John the Baptist are fast days, with wine and oil allowed.
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Fasts.isFastDay = args=>new Promise((resolve,reject)=>{
	// First determine if this is a Fast-free week, then go through the
	// calculation to determine fast strictness, if needed
	return Fasts.isFastFreePeriod(args)
		.then(isFF=>{
			if (isFF) {
				var ff = {
					fastingDay: false,
					wine: true,
					oil: true,
					dairy: true,
					meat: true,
					fish: true,
					strict: false,
					noFood: false,
					xerophagy : false // fruit,nut,bread,honey,vegetables boiled in water and salt
				};

				//Override fasting calculations with the fast free check.
				resolve(ff);

			} else {
				Fasts.calculateFastingRules(args)
					.then(resolve);
			}//else
		});//isFF
});


/**
 * * Determines if date is fast free
 *
 * args.month
 * args.momentDate
 * args.dayOfMonth
 * currentPeriods
 * args.periods
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Fasts.isFastFreePeriod = args=>new Promise((resolve,reject)=>{
	const calendar = args.calendar;
		args = utils.dateObj(args);
	var i,
		isFF = false;

	dates({year:args.year,calendar})
		.then(orthodoxDates=> {

			// **** Fixed Dates ****
			// Determine if it is the after Nativity fast-free period
			// 1/7 - 1/18
			if (args.month == 1 && args.dayOfMonth >= 7 && args.dayOfMonth <= 18) {
				isFF = true;
			}

			// **** Moveable Dates ****
			//---- Publican and Pharisee fast-free week ----
			var publicanPharisee = orthodoxDates.publicanPharisee,
				pfStart          = new Date(publicanPharisee),
				pfEnd            = new Date(publicanPharisee);
			pfEnd = new Date(pfEnd.setDate(pfEnd.getDate() + 7));

			//Is our request during publican pharisee fast-free week?
			if (moment(pfStart).isBefore(args.momentDate) && moment(pfEnd).isAfter(args.momentDate)) {
				isFF = true;
			}

			// Fetch the periods for the date
			periods.getPeriodsForDate({date:args.date,calendar})
				.then(currentPeriods=>{
					const max  = currentPeriods.length;

					// Exit if there are no current periods being passed in
					if (max == 0) { resolve(isFF);}

					// Loop through currentPeriods
					for (i = 0; i < max; i++) {
						switch (currentPeriods[i]) {
							case 'brightWeek' :
								isFF = true;
								break;
							case 'trinityWeek' :
								isFF = true;
								break;
							case 'weekOfPublicanPharisee' :
								isFF = true;
								break;
						}

						if (i + 1 >= max) {
							resolve(isFF);
						}
					}
			});
		});
});


/**
 * Export
 * @type {{}}
 */
export default Fasts;