var Promise      = require('bluebird'),
	_			 = require('underscore'),
	Psalter 	 = {};

import greatLent from './GreatLent';
import utils from './utils';

/**
 * Gets the kathisma readings for the specified day of the week by whether is Great Lent or not.
 *
 * args.isGreatLent
 * args.dayOfWeek
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Psalter.getKathismaNumbersByDate = args => new Promise((resolve,reject) => {
    var kathisma    = {};
    args = utils.dateObj(args);

    // Check if we're in Great Lent
    return greatLent.is({date:args.date,calendar:args.calendar})
        .then(is=>{
            if(is){
                kathisma.matins   = Psalter.getLentOrthrosKathisma(args.dayOfWeek);
                kathisma.firstHour = Psalter.getLentFirstHourKathisma(args.dayOfWeek);
                kathisma.thirdHour = Psalter.getLentThirdHourKathisma(args.dayOfWeek);
                kathisma.sixthHour = Psalter.getLentSixthHourKathisma(args.dayOfWeek);
                kathisma.ninthHour = Psalter.getLentNinthKathisma(args.dayOfWeek);
                kathisma.vespers   = Psalter.getLentVespersKathisma(args.dayOfWeek);
            }else{
                kathisma.matins = Psalter.getNonLentOrthrosKathisma(args.dayOfWeek);
                kathisma.vespers = Psalter.getNonLentVespersKathisma(args.dayOfWeek);
            }

            resolve(kathisma);
    });
});






//==========================//
// Kathisma Mappers
//==========================//
/**
 * Returns OrthrosKathisma Outside Lent
 * @param day
 * @returns {Array}
 */
Psalter.getNonLentOrthrosKathisma = function(day){
    var kathismaReq = [];

    // Map of Kathisma readings, outside Lent, for each day of the week, in Orthros
    switch(day){
        case 0 :
            kathismaReq.push(2,3);
            break;
        case 1 :
            kathismaReq.push(4,5);
            break;
        case 2 :
            kathismaReq.push(7,8);
            break;
        case 3 :
            kathismaReq.push(10,11);
            break;
        case 4 :
            kathismaReq.push(13,14);
            break;
        case 5 :
            kathismaReq.push(19,20);
            break;
        case 6 :
            kathismaReq.push(16,17);
            break;
        default :
            return false;
    }

    return kathismaReq;
};


/**
 * Returns Vespers Kathisma Outside Lent
 * @param day
 * @returns {Array}
 */
Psalter.getNonLentVespersKathisma = function(day){
    var kathismaReq = [];

    // Map of Kathisma readings, outside Lent, for each day of the week, in Vespers
    switch(day){
        case 0 :
            break;
        case 1 :
            kathismaReq.push(6);
            break;
        case 2 :
            kathismaReq.push(9);
            break;
        case 3 :
            kathismaReq.push(12);
            break;
        case 4 :
            kathismaReq.push(15);
            break;
        case 5 :
            kathismaReq.push(18);
            break;
        case 6 :
            kathismaReq.push(1);
            break;
        default :
            return false;
    }

    return kathismaReq;
};

/**
 * Get Lenten Orthros Kathisma
 * @param day
 * @returns {Array}
 */
Psalter.getLentOrthrosKathisma = function(day){
    var kathismaReq = [];

    // Map of Kathisma readings, outside Lent, for each day of the week, in Vespers
    switch(day){
        case 0 :
            kathismaReq.push(2,3);
            break;
        case 1 :
            kathismaReq.push(4,5,6);
            break;
        case 2 :
            kathismaReq.push(10,11,12);
            break;
        case 3 :
            kathismaReq.push(19,20,1);
            break;
        case 4 :
            kathismaReq.push(6,7,8);
            break;
        case 5 :
            kathismaReq.push(13,14,15);
            break;
        case 6 :
            kathismaReq.push(16,17);
            break;
        default :
            return false;
    }

    return kathismaReq;
};

/**
 * Get Lenten First Hour Kathisma
 * @param day
 * @returns {Array}
 */
Psalter.getLentFirstHourKathisma = function(day){
    var kathismaReq = [];

    // Map of Kathisma readings, outside Lent, for each day of the week, in Vespers
    switch(day){
        case 2 :
            kathismaReq.push(13);
            break;
        case 3 :
            kathismaReq.push(2);
            break;
        case 4 :
            kathismaReq.push(9);
            break;
        default :
            return false;
    }

    return kathismaReq;
};

/**
 * Get Lenten Third Hour Kathisma
 * @param day
 * @returns {Array}
 */
Psalter.getLentThirdHourKathisma = function(day){
    var kathismaReq = [];

    // Map of Kathisma readings, outside Lent, for each day of the week, in Vespers
    switch(day){
        case 1 :
            kathismaReq.push(7);
            break;
        case 2 :
            kathismaReq.push(14);
            break;
        case 3 :
            kathismaReq.push(3);
            break;
        case 4 :
            kathismaReq.push(10);
            break;
        case 5 :
            kathismaReq.push(19);
            break;
        default :
            return false;
    }

    return kathismaReq;
};


/**
 * Get Lenten Sixth Hour Kathisma
 * @param day
 * @returns {Array}
 */
Psalter.getLentSixthHourKathisma = function(day){
    var kathismaReq = [];

    // Map of Kathisma readings, outside Lent, for each day of the week, in Vespers
    switch(day){
        case 1 :
            kathismaReq.push(8);
            break;
        case 2 :
            kathismaReq.push(15);
            break;
        case 3 :
            kathismaReq.push(4);
            break;
        case 4 :
            kathismaReq.push(11);
            break;
        case 5 :
            kathismaReq.push(20);
            break;
        default :
            return false;
    }

    return kathismaReq;
};


/**
 * Get Lenten Ninth Hour KAthisma
 * @param day
 * @returns {Array}
 */
Psalter.getLentNinthKathisma = function(day){
    var kathismaReq = [];

    // Map of Kathisma readings, outside Lent, for each day of the week, in Vespers
    switch(day){
        case 1 :
            kathismaReq.push(9);
            break;
        case 2 :
            kathismaReq.push(16);
            break;
        case 3 :
            kathismaReq.push(5);
            break;
        case 4 :
            kathismaReq.push(12);
            break;
        default :
            return false;
    }

    return kathismaReq;
};


/**
 * Get Lenten Vespers KAthisma
 * @param day
 * @returns {Array}
 */
Psalter.getLentVespersKathisma = function(day){
    var kathismaReq = [];

    // Map of Kathisma readings, outside Lent, for each day of the week, in Vespers
    switch(day){
        case 1 :
            kathismaReq.push(18);
            break;
        case 2 :
            kathismaReq.push(18);
            break;
        case 3 :
            kathismaReq.push(18);
            break;
        case 4 :
            kathismaReq.push(18);
            break;
        case 5 :
            kathismaReq.push(18);
            break;
        case 6 :
            kathismaReq.push(1);
            break;
        default :
            return false;
    }

    return kathismaReq;
};


export default Psalter;