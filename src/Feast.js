const Promise   = require('bluebird'),
      dates     = require('./Dates'),
      //periods   = require('./Periods'),
      utils 	= require('./utils'),
    Feast = {};



/**
 * Retrieves Feast date
 *
 * orthodoxDates
 * feastNameSlug
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Feast.getFeastDateByNameSlug = args=>new Promise(function(resolve,reject){
    const {calendar,feastNameSlug,year} = args;
        args = utils.dateObj(args);

    // Get all periods for request date
    dates({year,calendar})
        .then(orthodoxDates=>{
            for(var key in orthodoxDates){
                if(key === feastNameSlug){
                    resolve(orthodoxDates[key]);
                }
            }
        });
});

/**
 * Determines if is 12 great, feast of Lord, or Theotokos
 * //http://orthodoxwiki.org/Classification_of_Feasts
 *
 * feastNameSlug
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Feast.getType = args=>new Promise(function(resolve,reject){
    const {feastNameSlug} = args;

    let types = {
        has          : false,
        feastOfFeasts: false,
        twelveGreat  : {
            is: false,
            of: undefined
        },
        firstClass   : {
            is: false,
            of: undefined
        },
        secondClass  : {
            is: false,
            of: undefined
        },
        thirdClass   : {
            is: false,
            of: undefined
        }
    };

    // Mark Pascha as feast of feasts
    if (feastNameSlug == 'pascha') {
        types.has = true;
        types.feastOfFeasts = true;
    }

    // Determine if this is one of the 12 great feasts
    switch (feastNameSlug) {
        case 'nativityTheotokos':
        case 'exaltationCross':
        case 'presentationTheotokos':
        case 'nativity':
        case 'theophany':
        case 'presentationChrist':
        case 'annunciation':
        case 'palmSunday':
        case 'ascension':
        case 'pentecost':
        case 'transfiguration':
        case 'dormition':
            types.has = true;
            types.twelveGreat.is = true;
            types.twelveGreat.of = feastNameSlug;
            break;
    }

    // Is this a feast of the Lord
    switch (feastNameSlug) {
        case 'exaltationCross':
        case 'nativity':
        case 'palmSunday':
        case 'ascension':
        case 'pentecost':
        case 'transfiguration':
        case 'theophany':// @todo Maybe this is not considered a feast of the Lord
            types.has = true;
            types.firstClass.is = true;
            types.firstClass.of = 'Lord';
            break;
    }

    // Is this a feast of the Theotokos
    switch (feastNameSlug) {
        case 'nativityTheotokos':
        case 'presentationTheotokos':
        case 'dormition':
        case 'presentationChrist':
        case 'annunciation':
            types.has = true;
            types.secondClass.is = true;
            types.secondClass.of = 'Theotokos';
            break;
    }

    resolve(types);
});


/**
 * Returns an obj of all the feasts
 *
 * orthodoxDates
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Feast.getAllForYear = args=>new Promise(function(resolve,reject){
    const {calendar,year} = args;
    let feasts = {};

    // Get all periods for request date
    dates({year,calendar})
        .then(orthodoxDates=> {

            for (var key in orthodoxDates) {
                if (key == 'palmSunday') { feasts['palmSunday'] = orthodoxDates[key];}
                if (key == 'pascha') { feasts['pascha'] = orthodoxDates[key];}
                if (key == 'pentecost') { feasts['pentecost'] = orthodoxDates[key];}
                if (key == 'allSaints') { feasts['allSaints'] = orthodoxDates[key];}
                if (key == 'samaritanWoman') { feasts['samaritanWoman'] = orthodoxDates[key];}
                if (key == 'ascension') { feasts['ascension'] = orthodoxDates[key];}
                if (key == 'nativity') { feasts['nativity'] = orthodoxDates[key];}
                if (key == 'theophany') { feasts['theophany'] = orthodoxDates[key];}
                if (key == 'presentationChrist') {feasts['presentationChrist'] = orthodoxDates[key];}
                if (key == 'annunciation') { feasts['annunciation'] = orthodoxDates[key];}
                if (key == 'feastPeterPaul') { feasts['feastPeterPaul'] = orthodoxDates[key];}
                if (key == 'transfiguration') { feasts['transfiguration'] = orthodoxDates[key];}
                if (key == 'dormition') { feasts['dormition'] = orthodoxDates[key];}
                if (key == 'beheadingBaptist') { feasts['beheadingBaptist'] = orthodoxDates[key];}
                if (key == 'nativityTheotokos') { feasts['nativityTheotokos'] = orthodoxDates[key];}
                if (key == 'exaltationCross') { feasts['exaltationCross'] = orthodoxDates[key];}
                if (key == 'presentationTheotokos') {feasts['presentationTheotokos'] = orthodoxDates[key];}
            }

            resolve(feasts);
        });
});


/**
 * Export
 * @type {{}}
 */
export default Feast;