const Promise   = require('bluebird'),
      utils 	= require('./utils'),
    WeeklyCycle = {};

/**
 * Returns the proper weekly cycle for the day
 *
 * args.dayOfWeek
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
WeeklyCycle.getByDate = args=>new Promise((resolve,reject)=>{
    args = utils.dateObj(args);

    let cycle = {
        resurrection    : false,
        angels          : false,
        stJohnTheBaptist: false,
        allProphets     : false,
        apostles        : false,
        stNicholas      : false,
        martyrs         : false,
        ascetics        : false,
        reposed         : false,
        cross           : false,
        theotokos       : false
    };

    switch (args.dayOfWeek) {
        case 0 :
            cycle.resurrection = true;
            break;
        case 1 :
            cycle.angels = true;
            break;
        case 2 :
            cycle.stJohnTheBaptist = true;
            cycle.allProphets = true;
            break;
        case 3 :
            cycle.cross = true;
            cycle.theotokos = true;
            break;
        case 4 :
            cycle.apostles = true;
            cycle.stNicholas = true;
            break;
        case 5 :
            cycle.cross = true;
            cycle.theotokos = true;
            break;
        case 6 :
            cycle.martyrs = true;
            cycle.ascetics = true;
            cycle.reposed = true;
            break;
    }

    resolve(cycle);
});


/**
 * Export
 * @type {{}}
 */
export default WeeklyCycle;