var Promise = require('bluebird');

/**
 * Based on the calculations of Carl Friedrich Gauss
 * Calculations in the Julian Calendar
 * calculatePascha(2044, 'new');
 * @param year
 * @param cal
 * @param cb
 */
export default function(args){
	return new Promise(function(resolve,reject){
		var a, b, c, d, e, f, g, h, calendarOffset;

		//Make new cal/old cal calculation
		calendarOffset = args.calendar == 'new' ? 13 : 0;

		a = args.year % 19;
		b = args.year % 4; //If leap year, this is always 0
		c = args.year % 7;
		d = ( 19*a + 15 ) % 30;
		e = ( (2*b)+(4*c)+(6*d)+6 ) % 7;

		//Calculate days after March 1st (Old Cal.)
		f = d + e + 21;

		g = new Date('3/1/' + args.year);
		h = g.setDate(g.getDate() + f + calendarOffset);

		resolve(new Date(h));
	});
}