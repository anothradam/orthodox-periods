let utils    		= require('./utils.js'),
    Promise      	= require('bluebird'),
    moment 			= require('moment'),
    dates 			= require('./Dates'),
    Color 	 		= {};



/**
* determines current vestment color by date
* @todo NOT FINISHED - Finish color calculations for ranks of feasts and feasts of st john the baptist, etc
* @param args
*/
Color.getByDate = args => new Promise((resolve,reject) => {
	const {calendar} = args;
		args = utils.dateObj(args);
		args.color = {
			gold        : true,//default val
			white       : false,
			red         : false,
			wineRed     : false,
			blue        : false,
			purple      : false,
			darkPurple  : false,
			black       : false,
			green       : false
		};

    // Check if it's a sunday or weekday here
    // @todo check if saturday color is differerent
   // if(utils.isSunday(date, true) || utils.isWeekday(date, true)){ color.gold = true;}


	// Get all periods for request date
	return dates({year:args.year,calendar})
		.then(orthodoxDates=>{
			args.periods = orthodoxDates;

			// Determine Current period color then reset and assign
			return Color.makeFeast(args)
				.then(Color.makeLent)
				.then(Color.makeHolyWeek)
				.then(Color.assignDefault)
				.then(res =>{
					resolve(res.color);
				});

		});
});//getColor




//======================================//
// Internal use below
//======================================//

/**
 * Ensures there is a true value & sets default if none
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Color.assignDefault = args => new Promise((resolve,reject) => {
	let hasColorAssigned = false;

	// Run through color obj to look for true vals
	for (let key in args.color) {
		if(args.color[key]){
			hasColorAssigned = true;
		}
	}

	// Assign default gold if no color was assigned
	if(!hasColorAssigned){
		args.color.gold = true;
	}

	resolve(args);
});



/**
 * Makes the color object
 *
 * args.periods
 * args.date
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Color.makeFeast = args => new Promise((resolve,reject) => {
	let color;

	let params = [
		{firstPeriod: 'nativityTheotokos', secondPeriod:'apodosisNativityTheotokos',color:'blue'},
		{firstPeriod: 'exaltationCross', secondPeriod:'apodosisExaltationCross',color:'wineRed'},
		{firstPeriod: 'protectionTheotokos', secondPeriod:'protectionTheotokos',color:'blue'},
		{firstPeriod: 'presentationTheotokos', secondPeriod:'apodosisPresentationTheotokos',color:'blue'},
		{firstPeriod: 'nativityFast', secondPeriod:'nativityFast',color:'red'},
		{firstPeriod: 'nativity', secondPeriod:'apodosisNativity',color:'white'},
		{firstPeriod: 'synaxisTheotokos', secondPeriod:'synaxisTheotokos',color:'white'},
		{firstPeriod: 'circumcision', secondPeriod:'circumcision',color:'white'},
		{firstPeriod: 'theophany', secondPeriod:'apodosisTheophany',color:'white'},
		{firstPeriod: 'presentationChrist', secondPeriod:'apodosisPresentationChrist',color:'blue'},
		{firstPeriod: 'annunciation', secondPeriod:'annunciation',color:'blue'},
		{firstPeriod: 'palmSunday', secondPeriod:'palmSunday',color:'green'},
		{firstPeriod: 'pascha', secondPeriod:'apodosisPascha',color:'white'},
		{firstPeriod: 'ascension', secondPeriod:'apodosisAscension',color:'white'},
		{firstPeriod: 'pentecost', secondPeriod:'apodosisPentecost',color:'green'},
		{firstPeriod: 'apostlesFast', secondPeriod:'apostlesFast',color:'red'},
		{firstPeriod: 'feastPeterPaul', secondPeriod:'feastPeterPaul',color:'red'},
		{firstPeriod: 'transfiguration', secondPeriod:'apodosisTransfiguration',color:'white'},
		{firstPeriod: 'dormitionFast', secondPeriod:'dormitionFast',color:'blue'},
		{firstPeriod: 'transfiguration', secondPeriod:'transfiguration',color:'white'},
		{firstPeriod: 'dormition', secondPeriod:'apodosisDormition',color:'blue'}
	];

	//// Reset colors then run through list
	//return resetColors(args.color)
	//	.then(res =>{
			let count = 0,
				max = params.length;

			// Apply reset
			//args.color = res;

			// Start recursive logic
			iterate();
			function iterate(){
				if(count < max) {
					if (utils.dateWithinPeriodRange(args, params[count].firstPeriod, params[count].secondPeriod)) {
						color = params[count].color;
					}

					count++;
					iterate();
				}else{
					//set the determined color from above
					if(color){
						return resetColors(args.color)
							.then(res => {
								args.color = res;
								args.color[color] = true;

								resolve(args);
							});
					}else{
						resolve(args);
					}
				}
			}
		//});

	//// Color rubrics - Async
	//utils.dateWithinPeriodRange(args,'nativityTheotokos','apodosisNativityTheotokos')
	//    .then(is=>{if(is){resetColors(); color.blue = true;}});
	//utils.dateWithinPeriodRange(args,'exaltationCross','apodosisExaltationCross')
	//    .then(is=>{if(is){resetColors(); color.wineRed = true;}});
	//utils.dateWithinPeriodRange(args,'protectionTheotokos','protectionTheotokos')
	//    .then(is=>{if(is){resetColors(); color.blue = true;}});
	//utils.dateWithinPeriodRange(args,'presentationTheotokos','apodosisPresentationTheotokos')
	//    .then(is=>{if(is){resetColors(); color.blue = true;}});
	//utils.dateWithinPeriodRange(args,'nativityFast','nativityFast')
	//    .then(is=>{if(is){resetColors(); color.red = true;}});
	//utils.dateWithinPeriodRange(args,'nativity','apodosisNativity')
	//    .then(is=>{if(is){resetColors(); color.white = true;}});
	//utils.dateWithinPeriodRange(args,'synaxisTheotokos','synaxisTheotokos')
	//    .then(is=>{if(is){resetColors(); color.white = true;}});
	//utils.dateWithinPeriodRange(args,'circumcision','circumcision')
	//    .then(is=>{if(is){resetColors(); color.white = true;}});
	//utils.dateWithinPeriodRange(args,'theophany','apodosisTheophany')
	//    .then(is=>{if(is){resetColors(); color.white = true;}});
	//utils.dateWithinPeriodRange(args,'presentationChrist','apodosisPresentationChrist')
	//    .then(is=>{if(is){resetColors(); color.blue = true;}});
	//utils.dateWithinPeriodRange(args,'annunciation','annunciation')
	//    .then(is=>{if(is){resetColors(); color.blue = true;}});
	//utils.dateWithinPeriodRange(args,'palmSunday','palmSunday')
	//    .then(is=>{if(is){resetColors(); color.green = true;}});
	//utils.dateWithinPeriodRange(args,'pascha','apodosisPascha')
	//    .then(is=>{if(is){resetColors(); color.white = true;}});
	//utils.dateWithinPeriodRange(args,'ascension','apodosisAscension')
	//    .then(is=>{if(is){resetColors(); color.white = true;}});
	//utils.dateWithinPeriodRange(args,'pentecost','apodosisPentecost')
	//    .then(is=>{if(is){resetColors(); color.green = true;}});
	//utils.dateWithinPeriodRange(args,'apostlesFast','apostlesFast')
	//    .then(is=>{if(is){resetColors(); color.red = true;}});
	//utils.dateWithinPeriodRange(args,'feastPeterPaul','feastPeterPaul')
	//    .then(is=>{if(is){resetColors(); color.red = true;}});
	//utils.dateWithinPeriodRange(args,'transfiguration','apodosisTransfiguration')
	//    .then(is=>{if(is){resetColors(); color.white = true;}});
	//utils.dateWithinPeriodRange(args,'dormitionFast','dormitionFast')
	//    .then(is=>{if(is){resetColors(); color.blue = true;}});
	//utils.dateWithinPeriodRange(args,'transfiguration','transfiguration')
	//    .then(is=>{if(is){resetColors(); color.white = true;}});// feast overrides dormition fast
	//utils.dateWithinPeriodRange(args,'dormition','apodosisDormition')
	//    .then(is=>{if(is){resetColors(); color.blue = true;}});
});


/**
 * args.isWeekday
 * args.isSaturday
 * args.isSunday
 * args.color
 * args.periods
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Color.makeLent = args => new Promise((resolve,reject) => {
	if(utils.dateWithinPeriodRange(args,'greatLent','greatLent')){
		return resetColors(args.color)
			.then(res =>{
				args.color = res;

				// Weekdays in Lent
				if (args.isWeekday) {
					args.color.black = true;
					args.color.darkPurple = true;
				}

				// Sat,sun in Lent
				if (args.isSaturday || args.isSunday) {
					args.color.purple = true;
				}

				resolve(args);
			});
	}else{
		resolve(args);
	}
});


/**
 * args.dayOfWeek
 * args.color
 * args.periods
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Color.makeHolyWeek = args => new Promise((resolve,reject) => {
	if(utils.dateWithinPeriodRange(args,'holyWeek','holyWeek')){
		// Reset colors and assign
		return resetColors(args.color)
			.then(res => {
				args.color = res;

				// Set Holy Week colors
				args.color.black = true;
				args.color.darkPurple = true;

				// Thurs & Sat Holy Week
				switch(args.dayOfWeek){
					case 4 :
						args.color.black = false;
						args.color.darkPurple = false;
						args.color.red = true;
						break;
					case 6 :
						args.color.black = false;
						args.color.darkPurple = false;
						args.color.white = true;
						break;
				}

				resolve(args);
			});
	}else{
		resolve(args);
	}//isHW
});


/**
 * utility to reset color object for overriding normal weekday and sunday colors with period color
 *
 * takes in args.color & returns same
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
function resetColors(args){
	return new Promise((resolve,reject)=> {
		for (let key in args) {
			args[key] = false;
		}

		resolve(args);
	});
}

export default Color;