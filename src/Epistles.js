'use strict'
import _ from 'underscore';
import Dates from './Dates';
import Periods from './Periods';
import GreatLent from './GreatLent';
import utils from './utils';

var moment  = require('moment'),
	Promise = require('bluebird'),
	epistlesCollection = require('./epistles.json'),
	l = console.log,

	Epistles = {

		/**
		 * Retrieves a date's epistle reading
		 *
		 * @param args
		 */
		getByDate : args => new Promise((resolve,reject) => {
			const {calendar} = args;
			args = utils.dateObj(args);
			args.calendar = calendar;

			/**
			 * 1. Checks for a match on exact date
			 * 2. Check for weekAfterPentecost in args
			 * 3. Check for weekAfterPascha in args
			 */
			Promise.all([
					Epistles.getByFixedDate(args),
					Periods.getWeekAfterPentecost({date:args.date,calendar:args.calendar}),
					Periods.getWeekAfterPascha({date:args.date,calendar:args.calendar}),
					Dates({year:args.year,calendar:args.calendar}),
					GreatLent.is({date:args.date,calendar:args.calendar})
				])
				.then(res=> {
					let resultArray = [],
						fixedDateReading = res[0];
					args.weekAfterPentecost = res[1].week;
					args.weekAfterPascha = parseInt(res[2].week,10);
					args.periods = res[3];
					args.isGreatLent = res[4];

					/**
					 * Get by fixed date
					 * @todo commented out.Can't confirm accuracy of fixed dates
					 */
					//if (fixedDateReading.length > 0) {
					//	resultArray = resultArray.concat(_makeReadings(fixedDateReading));
					//}

					/**
					 * Get by weekAfterPentecost, then weekAfterPascha, then default
					 */
					//if (args.weekAfterPentecost > -1){
						return Epistles.getByDayAndWeekAfterPentecost(args)
							.then(result=>{
								if (result.length) {
									resultArray = resultArray.concat(_makeReadings(result));
									resolve(resultArray);

								}else{
									//l(args.weekAfterPentecost,args.weekAfterPascha);
									args.weekAfterPascha = args.weekAfterPascha;

									return Epistles.getByDayAndWeekAfterPascha(args)
										.then(function (reading) {
											if (reading.length) {
												resultArray = resultArray.concat(_makeReadings(reading));
											}

											resolve(resultArray);
										}, console.log);
								}
							}, console.log)
					//}
				});
		}),

		/**
		 * Returns all epistles
		 *
		 * args.periods
		 *
		 * @param args
		 * @returns {bluebird|exports|module.exports}
		 */
		getAll: args=>new Promise((resolve, reject)=> {
			resolve(epistlesCollection);
		}),//getEpistles

		/**
		 * Get epistle by week after pentecost and day requested
		 *
		 * args.weekAfterPentecost
		 * args.dayOfWeek
		 *
		 * @param args
		 * @returns {bluebird|exports|module.exports}
		 */
		getByDayAndWeekAfterPentecost : args=>new Promise(function (resolve, reject) {
			resolve(_.filter(epistlesCollection,item=>{
				if(args.dayOfWeek == 0){
					return (item.weekAfterPentecost+1) == args.weekAfterPentecost && item.dayOfWeek == args.dayOfWeek;
				}
				return item.weekAfterPentecost == args.weekAfterPentecost && item.dayOfWeek == args.dayOfWeek;
			}));
		}),


		/**
		 * Gets reading by day and week after Pascha
		 *
		 * args.dayOfWeek
		 * args.weekAfterPascha
		 *
		 * @param args
		 * @returns {bluebird|exports|module.exports}
		 */
		getByDayAndWeekAfterPascha : args=>new Promise(function (resolve, reject) {
			resolve(_.filter(epistlesCollection,item=>{
				return item.weekAfterPascha == args.weekAfterPascha && item.dayOfWeek == args.dayOfWeek;
			}));
		}),


		/**
		 * Get's a document by fixed date match
		 *
		 * args.month
		 * args.dayOfMonth
		 *
		 * @param args
		 * @returns {bluebird|exports|module.exports}
		 */
		getByFixedDate : args=>new Promise(function (resolve, reject) {
			resolve(_.filter(epistlesCollection,item=>{
				return item.fixedDateMonth == args.month && item.fixedDateDay == args.dayOfMonth;
			}));
		}),

		/**
		 * Gets the Resurrection Epistle (eothina) requestedepistle by service
		 * @param args
		 * @returns {bluebird|exports|module.exports}
		 */
		getByService : args=>new Promise(function (resolve, reject) {
			resolve(_.filter(epistlesCollection,item=>{
				return item.service == args.service;
			}));
		}),

		/**
		 * Return results by section number
		 * @param args
		 */
		getBySection : args=>new Promise(function (resolve, reject) {
			resolve(_.filter(epistlesCollection,item=>{
				return item.section == args.section;
			}));
		})
	};
/**
 * Export
 * @type {{}}
 */
export default Epistles;

/**
 * Utility for building an array of readings
 * @param reading
 * @returns {Array}
 * @private
 */
function _makeReadings(reading) {
	var a = [];
	for (var i = 0; i < reading.length; i++) {
		a.push({
			reading: reading[i].passage,
			section: reading[i].section,
			link: utils.makeScriptureLink(encodeURI(reading[i].passage)),
			service : reading[i].service || false
		});
	}
	return a;
}