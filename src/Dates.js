/*
 * This "hash table" calculates and returns the dates for
 * all the moveable feasts and periods of the liturgical year
 *
 * Pascha, Ascension, Pentecost, Palm Sunday have no forefeast
 */

//Dependencies
var getPascha 	= require('./Pascha');
var Promise 	= require('bluebird');
import utils from './utils';
import moment from 'moment';

export default function(args){
	return new Promise(function(resolve,reject) {
		var year = args.year;

		//Retrieve pascha date for given year
		// args.year, args.calendar
		getPascha(args)
			.then(function (pascha){
				args.pascha = pascha;

				var periods = {
					/**
					 * Pascha
					 */
					pascha : singleFixed({date:args.pascha}),

					/**
					 * -77 days from pascha
					 */
					zacchaeusSunday : singleMoveable({pascha:args.pascha,offset:77,polarity:false}),

					/**
					 * -70 days from pascha
					 * This date is also Triodion Start
					 */
					publicanPharisee : singleMoveable({pascha:args.pascha,offset:70,polarity:false}),

					/**
					 * weekOfPublicanPharisee is fast free from Sunday to the following Saturday just before Sunday of the Prodigal Son
					 */
					weekOfPublicanPharisee  : rangeMoveable({pascha:args.pascha,offset:{start:70,end:64,polarity:false}}),

					/**
					 * -70 days from pascha
					 *This date is the Publican & The Pharisee
					 * Ends Midnight Office on the eve of Pascha
					 */
					triodion  : rangeMoveable({pascha:args.pascha,offset:{start:70,end:1,polarity:false}}),

					/**
					 * -15 days from Great Lent Start
					 * @returns {Date}
					 */
					sundayProdigalSon : singleMoveable({pascha:args.pascha,offset:63,polarity:false}),

					/**
					 * Meatfare Sunday of the last judgement
					 * -8 days from Great Lent Start
					 * Last non-fasting day
					 * @returns {Date}
					 */
					sundayLastJudgementMeatfare : singleMoveable({pascha:args.pascha,offset:56,polarity:false}),

					/**
					 * -14 days from Great Lent Start
					 * Monday - First day of Meatfare.
					 * Day after Prodigal Son
					 * -8 days from Great Lent Start
					 * Also sunday of the last judgement
					 * @returns {{}|*}
					 */
					meatfare : rangeMoveable({pascha:args.pascha,offset:{start:62,end:56,polarity:false}}),

					/**
					 * -7 days from Great Lent Start
					 * -1 days from Great Lent Start
					 * @returns {{}|*}
					 */
					cheesefare : rangeMoveable({pascha:args.pascha,offset:{start:55,end:49,polarity:false}}),

					/**
					 * Casting of Adam from paradise
					 * Also forgiveness Sunday
					 * -1 days from Great Lent Start
					 * @returns {Date}
					 */
					sundayCheesefareCastingAdam :  singleMoveable({pascha:args.pascha,offset:49,polarity:false}),
					forgivenessSunday :  singleMoveable({pascha:args.pascha,offset:49,polarity:false}),
					/**
					 *
					 * @returns {{}|*}
					 */
					firstWeekGreatLent : rangeMoveable({pascha:args.pascha,offset:{start:48,end:42,polarity:false}}),

					/**
					 * +22 days from triodionStart
					 * -48 days from Pascha
					 * Duplicate of triodionEnd
					 * Ends Midnight Office on the eve of Pascha
					 * @returns {{}|*}
					 */
					greatLent : rangeMoveable({pascha:args.pascha,offset:{start:48,end:1,polarity:false}}),

					/**
					 * -6 days from Pascha
					 * Duplicate of triodionEnd
					 * Ends Midnight Office on the eve of Pascha
					 * @returns {{}|*}
					 */
					holyWeek : rangeMoveable({pascha:args.pascha,offset:{start:6,end:1,polarity:false}}),

					/**
					 * -42 days from pascha
					 */
					sundayOfOrthodoxy :  singleMoveable({pascha:args.pascha,offset:42,polarity:false}),

					/**
					 * -35 days from pascha
					 */
					sundayOfStGregoryPalamas :  singleMoveable({pascha:args.pascha,offset:35,polarity:false}),

					/**
					 * -21 days from pascha
					 */
					sundayOfStJohnClimacus :  singleMoveable({pascha:args.pascha,offset:21,polarity:false}),

					/**
					 * -14 days from pascha
					 */
					sundayOfStMaryOfEgypt :  singleMoveable({pascha:args.pascha,offset:14,polarity:false}),

					/**
					 * -8 days from pascha
					 */
					lazarusSaturday :  singleMoveable({pascha:args.pascha,offset:8,polarity:false}),

					/**
					 * Sunday before Pascha
					 * @returns {Date}
					 */
					palmSunday : singleMoveable({pascha:args.pascha,offset:7,polarity:false}),

					/**
					 * Starts Monday after Pascha
					 * +7 Days from Pascha
					 * @returns {{}|*}
					 */
					brightWeek : rangeMoveable({pascha:args.pascha,offset:{start:1,end:6,polarity:true}}),

					/**
					 * +8 days from Pascha
					 * This is the Sunday after Pascha
					 * Tone 1 begins again here
					 * @returns {Date}
					 */
					thomasSunday : singleMoveable({pascha:args.pascha,offset:7,polarity:true}),

					/**
					 * +14 days from Pascha
					 * @returns {Date}
					 */
					sundayOfTheMyrrhBearingWomen : singleMoveable({pascha:args.pascha,offset:14,polarity:true}),

					/**
					 * +21 days from Pascha
					 * @returns {Date}
					 */
					sundayOfTheParalytic : singleMoveable({pascha:args.pascha,offset:21,polarity:true}),

					/**
					 * +24 days after Pascha
					 * @returns {Date}
					 */
					midfeastOfPentecost : singleMoveable({pascha:args.pascha,offset:24,polarity:true}),

					/**
					 * 5th Sunday after Pascha
					 * +28 days after Pascha
					 * @returns {Date}
					 */
					sundayOfTheSamaritanWoman : singleMoveable({pascha:args.pascha,offset:28,polarity:true}),

					/**
					 * +35 days after Pascha
					 * @returns {Date}
					 */
					sundayOfTheBlindMan : singleMoveable({pascha:args.pascha,offset:35,polarity:true}),

					/**
					 * Pascha-today is the Afterfeast
					 * +40 days after Pascha
					 * 1 day before Ascension
					 * @returns {Date}
					 */
					apodosisPascha : singleMoveable({pascha:args.pascha,offset:38,polarity:true}),

					/**
					 * Forefeast of Ascension
					 * +40 days after Pascha
					 * 1 day before Ascension (same as apodosisPascha)
					 * @returns {{}|*}
					 */
					forefeastAscension: rangeMoveable({pascha:args.pascha,offset:{start:38,end:38,polarity:true}}),

					/**
					 * Ends 1 day before Ascension
					 * @returns {{}|*}
					 */
					afterfeastPascha : rangeMoveable({pascha:args.pascha,offset:{start:1,end:38,polarity:true}}),

					/**
					 * 40 Days after Pascha
					 * @returns {Date}
					 */
					ascension : singleMoveable({pascha:args.pascha,offset:39,polarity:true}),

					/**
					 *
					 * @returns {{}|*}
					 */
					afterfeastAscension : rangeMoveable({pascha:args.pascha,offset:{start:40,end:47,polarity:true}}),

					/**
					 * +42 days after Pascha
					 * @returns {Date}
					 */
					sundayOfTheFathersOfThe1StEcumenicalCouncil : singleMoveable({pascha:args.pascha,offset:42,polarity:true}),

					/**
					 * 7 Days after Ascension
					 * Afterfeast of Ascension is Acension until today
					 * @returns {Date}
					 */
					apodosisAscension : singleMoveable({pascha:args.pascha,offset:47,polarity:true}),

					/**
					 * 50 days after Pascha
					 * @returns {Date}
					 */
					pentecost : (function(){
						return new Promise(function(resolve,reject) {
							//50 days after Pascha
							var p = new Date(args.pascha);
							p.setDate(p.getDate() + 49);

							resolve(p);
						});
					})(),

					/**
					 * The Monday after Pentecost
					 */
					mondayOfHolySpirit : (function(){
						return new Promise(function(resolve,reject) {
							//51 days after Pascha
							var p = new Date(args.pascha);
							p.setDate(p.getDate() + 50);

							resolve(p);
						});
					})(),

					/**
					 * Week after Pentecost
					 * @returns {{}|*}
					 */
					trinityWeek : rangeMoveable({pascha:args.pascha,offset:{start:50,end:56,polarity:true}}),

					/**
					 * Essentially Trinity week -1
					 * @returns {{}|*}
					 */
					afterfeastPentecost : rangeMoveable({pascha:args.pascha,offset:{start:50,end:55,polarity:true}}),

					/**
					 *
					 * @returns {Date}
					 */
					apodosisPentecost : singleMoveable({pascha:args.pascha,offset:55,polarity:true}),

					/**
					 * 56 Days after Pascha
					 * 7 days after pentecost
					 * @returns {Date}
					 */
					sundayOfAllSaints : singleMoveable({pascha:args.pascha,offset:56,polarity:true}),

					/**
					 * 63 Days after Pascha
					 * 14 days after pentecost
					 * @returns {Date}
					 */
					sundayOfAllSaintsOfNorthAmerica : singleMoveable({pascha:args.pascha,offset:63,polarity:true}),

					/**
					 * 77 Days after Pascha ?
					 * Sunday closest to July 16th OC July 29th NC
					 * @returns {Date}
					 */
					fathersOfTheFirstSixCouncils : new Promise(function(resolve,reject) {

						// Get date for both Sunday before and Sunday after. Diff each with the original date to see who is closest
						// Use moment diff
						const fixedDate = new Date('7/29/' + year),
							  before = moment(dayOfWeekFromFixed({dayOfWeek:'Sun',direction:'before',fixedDate})).diff(moment(fixedDate),'days'),
							  beforeDays = before < 0 ? -before : before,
							  after = moment(dayOfWeekFromFixed({dayOfWeek:'Sun',direction:'after',fixedDate})).diff(moment(fixedDate),'days'),
							  afterDays = after < 0 ? -after : after;

						if(beforeDays > afterDays){
							resolve(dayOfWeekFromFixed({dayOfWeek:'Sun',direction:'after',fixedDate}));
						}else if(beforeDays < afterDays){
							resolve(dayOfWeekFromFixed({dayOfWeek:'Sun',direction:'before',fixedDate}));
						}else{
							// If it falls on a Sunday, celebrate it here
							resolve(fixedDate);
						}
					}),

					/**
					 * 2nd Sunday after Pentecost
					 * This is also All Saints Day
					 * NOTE***Pentecost to All Saints is Fast Free
					 * @returns {{}|*}
					 */
					pentecostarion : rangeMoveable({pascha:args.pascha,offset:{start:0,end:55,polarity:true}}),

					/**
					 * Day before the nativty fast begins
					 */
					eveOfTheNativityFast : (function () {
						return new Promise(function(resolve,reject) {
							let followingYear = typeof year === "number" ? year + 1 : parseInt(year,10)+1,
								s = new Date('1/7/' + followingYear);

							//-41 days before Nativity
							s.setDate(s.getDate() - 41);

							resolve(s);
						});
					})(),

					/**
					 * -40 days before Nativity
					 * Last day is day before Nativity
					 * @returns {{}|*}
					 */
					nativityFast : (function () {
						return new Promise(function(resolve,reject) {

							var s = new Date('1/7/' + year);
							var e = new Date('1/7/' + year);
							var range = {};

							//-40 days before Nativity
							s.setDate(s.getDate() - 40);
							//Last day is day before Nativity
							e.setDate(e.getDate() - 1);
							range.start = s;
							range.end = e;
							range.followingYear = getNativityFastForFollowingYear(year);
							resolve(range);
						});
					})(),

					/**
					 * 2 sundays before nativity
					 */
					secondSundayBeforeNativity : new Promise(function(resolve,reject) {
						const sundayBefore = dayOfWeekFromFixed({
							dayOfWeek:'Sun',
							direction:'before',
							fixedDate:new Date('1/7/' + year)
						});

						resolve(dayOfWeekFromFixed({
							dayOfWeek:'Sun',
							direction:'before',
							fixedDate:sundayBefore
						}));
					}),

					/**
					 * 2 sundays before nativity
					 */
					sundayOfTheForefathers : new Promise(function(resolve,reject) {
						const sundayBefore = dayOfWeekFromFixed({
							dayOfWeek:'Sun',
							direction:'before',
							fixedDate:new Date('1/7/' + year)
						});

						resolve(dayOfWeekFromFixed({
							dayOfWeek:'Sun',
							direction:'before',
							fixedDate:sundayBefore
						}));
					}),

					/**
					 * 1 sunday before nativity
					 */
					sundayBeforeNativity : new Promise(function(resolve,reject) {
						resolve(dayOfWeekFromFixed({
							dayOfWeek:'Sun',
							direction:'before',
							fixedDate:new Date('1/7/' + year)
						}));
					}),

					/**
					 * 1 sunday before nativity
					 */
					sundayOfTheFathers : new Promise(function(resolve,reject) {
						resolve(dayOfWeekFromFixed({
							dayOfWeek:'Sun',
							direction:'before',
							fixedDate:new Date('1/7/' + year)
						}));
					}),

					/**
					 * 5 days before Nativity
					 * @returns {{}}
					 */
					forefeastNativity : rangeFixed({start:'1/2/' + year,end:'1/6/' + year}),

					/**
					 * Nativity
					 * @returns {Date}
					 */
					nativity : (function(){
						return new Promise(function(resolve,reject) {
							var n = new Date('1/7/' + year);

							resolve(n);
						});
					})(),

					/**
					 * Synaxis of the Theotokos
					 */
					synaxisTheotokos : singleFixed({date:'1/8/' + year}),

					/**
					 * 1 sunday after nativity
					 */
					sundayAfterNativity : new Promise(function(resolve,reject) {
						resolve(dayOfWeekFromFixed({
							dayOfWeek:'Sun',
							direction:'after',
							fixedDate:new Date('1/7/' + year)
						}));
					}),

					/**
					 * 1 sunday after nativity
					 * commemorated on the Sunday after the Nativity.
					 * If there is no Sunday between December 25 OC and January 1 OC(when the 25th falls on a Sunday), the feast is moved to December 26 OC
					 */
					josephTheBetrothed_DavidTheKing_JamesTheBrotherOfTheLord : new Promise(function(resolve,reject) {
						const nativity = moment(new Date('1/7/'+year)),
							nativityDayOfWeek = nativity.day();//4

						if(nativityDayOfWeek === 0){
							resolve(new Date('1/08/'+year));
						}else{
							resolve(dayOfWeekFromFixed({
								dayOfWeek:'Sun',
								direction:'after',
								fixedDate:new Date('1/7/' + year)
							}));
						}
					}),

					/**
					 * Circumcision
					 */
					circumcision : singleFixed({date:'1/14/' + year}),

					/**
					 * Afterfeast of Nativity
					 */
					afterfeastNativity : rangeFixed({start:'1/8/' + year,end:'1/13/' + year}),

					/**
					 * Apodosis of Nativity
					 * @returns {Date}
					 */
					apodosisNativity : singleFixed({date:'1/13/' + year}),

					/**
					 * Sunday before Theophany
					 */
					sundayBeforeTheophany : new Promise(function(resolve,reject) {
						resolve(dayOfWeekFromFixed({
							dayOfWeek:'Sun',
							direction:'before',
							fixedDate:new Date('1/19/' + year)
						}));
					}),

					/**
					 * 4 days before Theophany
					 * @returns {{}}
					 */
					forefeastTheophany : rangeFixed({start:'1/15/' + year,end:'1/18/' + year}),

					/**
					 * Theophany
					 * @returns {Date}
					 */
					theophany : singleFixed({date:'1/19/' + year}),

					/**
					 * Afterfeast Theophany
					 * @returns {{}}
					 */
					afterfeastTheophany : rangeFixed({start:'1/20/' + year,end:'1/27/' + year}),

					/**
					 * Sunday after Theophany
					 */
					sundayAfterTheophany : new Promise(function(resolve,reject) {
						resolve(dayOfWeekFromFixed({
							dayOfWeek:'Sun',
							direction:'after',
							fixedDate:new Date('1/19/' + year)
						}));
					}),

					/**
					 * Apodosis Theophany
					 */
					apodosisTheophany : singleFixed({date:'1/27/' + year}),

					/**
					 * Forefeast of the PResentation of Christ
					 */
					forefeastPresentationChrist : singleFixed({date:'2/14/' + year}),

					/**
					 * Presentation of Christ
					 */
					presentationChrist : singleFixed({date:'2/15/' + year}),

					/**
					 * NOTE: The afterfeast of the Presentation
					 * of Christ may be shortened or omitted
					 * altogether if February 2 falls on or
					 * after the Sunday of the Publican and Pharisee.
					 * @returns {{}}
					 */
					afterfeastPresentationChrist : rangeFixed({start:'2/16/' + year,end:'2/22/' + year}),

					/**
					 * apodosisPresentationChrist
					 */
					apodosisPresentationChrist : singleFixed({date:'2/22/' + year}),

					/**
					 * forefeastAnnunciation
					 */
					forefeastAnnunciation : singleFixed({date:'4/6/' + year}),

					/**
					 * annunciation
					 */
					annunciation : singleFixed({date:'4/7/' + year}),

					/**
					 * apostlesFast
					 * @returns {{}}
					 */
					apostlesFast : (function () {
						var self = this;
						return new Promise(function(resolve,reject){
							// Pentecost date
							var p = new Date(args.pascha);
							p.setDate(p.getDate() + 49);

							var s = new Date(p),
								range = {};

							//2nd Monday after Pentecost
							s.setDate(s.getDate() + 8);
							range.start = s;
							range.end = new Date('7/11/' + year);

							resolve(range);
						});
					})(),

					/**
					 * feastPeterPaul
					 * @returns {Date}
					 */
					feastPeterPaul : singleFixed({date:'7/12/' + year}),

					/**
					 * forefeastTransfiguration
					 * @returns {Date}
					 */
					forefeastTransfiguration : singleFixed({date:'8/18/' + year}),

					/**
					 * transfiguration
					 * @returns {Date}
					 */
					transfiguration : singleFixed({date:'8/19/' + year}),

					/**
					 * afterfeastTransfiguration
					 */
					afterfeastTransfiguration : rangeFixed({start:'8/20/' + year,end:'8/26/' + year}),

					/**
					 * apodosisTransfiguration
					 */
					apodosisTransfiguration : singleFixed({date:'8/26/' + year}),

					/**
					 * dormitionFast
					 */
					dormitionFast : rangeFixed({start:'8/14/' + year,end:'8/27/' + year}),

					/**
					 * forefeastDormition
					 */
					forefeastDormition : singleFixed({date:'8/27/' + year}),

					/**
					 * dormition
					 */
					dormition : singleFixed({date:'8/28/' + year}),

					/**
					 * afterfeastDormition
					 */
					afterfeastDormition : rangeFixed({start:'8/29/' + year,end:'9/5/' + year}),

					/**
					 * apodosisDormition
					 */
					apodosisDormition : singleFixed({date:'9/5/' + year}),

					/**
					 * beheadingBaptist
					 */
					beheadingBaptist : singleFixed({date:'9/11/' + year}),

					/**
					 * Church New Year
					 */
					indiction : singleFixed({date:'9/14/' + year}),

					/**
					 * forefeastNativityTheotokos
					 */
					forefeastNativityTheotokos : singleFixed({date:'9/20/' + year}),

					/**
					 * nativityTheotokos
					 */
					nativityTheotokos : singleFixed({date:'9/21/' + year}),

					/**
					 * afterfeastNativityTheotokos
					 */
					afterfeastNativityTheotokos : rangeFixed({start:'9/22/' + year,end:'9/25/' + year}),

					/**
					 * apodosisNativityTheotokos
					 */
					apodosisNativityTheotokos : singleFixed({date:'9/25/' + year}),

					/**
					 * Sunday before the cross
					 */
					sundayBeforeTheExaltationOfTheCross : new Promise(function(resolve,reject) {
						resolve(dayOfWeekFromFixed({dayOfWeek:'Sun',direction:'before',fixedDate:new Date('9/27/' + year)}));
					}),

					/**
					 * forefeastExaltationCross
					 */
					forefeastExaltationCross : singleFixed({date:'9/26/' + year}),

					/**
					 * exaltationCross
					 */
					exaltationCross : singleFixed({date:'9/27/' + year}),

					/**
					 * Sunday after the cross
					 */
					sundayAfterTheExaltationOfTheCross : new Promise(function(resolve,reject) {
						resolve(dayOfWeekFromFixed({dayOfWeek:'Sun',direction:'after',fixedDate:new Date('9/27/' + year)}));
					}),

					/**
					 * 8 days after exaltation begin the lukan jump calculations
					 */
					beginLukanJump : new Promise(function(resolve,reject) {
						resolve(dayOfWeekFromFixed({dayOfWeek:'Mon',direction:'after',fixedDate:new Date('9/27/' + year)}));
					}),

					/**
					 * 8 days after exaltation begin the lukan jump calculations + 19 weeks
					 */
					endLukanJump : new Promise(function(resolve,reject) {
						let d = new Date(dayOfWeekFromFixed({dayOfWeek:'Mon',direction:'after',fixedDate:new Date('9/27/' + year)})),
							dDayOfWeek = d.getDay(),
							endOfJump = new Date(d.setDate(d.getDate() + (19*7)));

						// According to Alexandr Andreev this is the real end of the Lukan Jump - Sunday before the Sunday of teh prodigal son
						//zacchaeusSunday : singleMoveable({pascha:args.pascha,offset:77,polarity:false}),

						// Ends on the Monday following the 19th week
						// If end date is a mOnday, just return that date otherwise get the date for the following Monday
						if(dDayOfWeek === 1){
							resolve(endOfJump);
						}else{
							resolve(dayOfWeekFromFixed({dayOfWeek:'Mon',direction:'after',fixedDate:endOfJump}));
						}
					}),

					/**
					 * afterfeastExaltationCross
					 */
					afterfeastExaltationCross : rangeFixed({start:'9/28/' + year,end:'10/4/' + year}),

					/**
					 * apodosisExaltationCross
					 */
					apodosisExaltationCross : singleFixed({date:'10/4/' + year}),

					/**
					 * October 24th NC or the Sunday before if it is thursday or less OR Sunday after if greater than thursday
					 */
					sundayOfTheFathersOfThe7thEcumenicalCouncil : new Promise(function(resolve,reject) {
						const fixedDate = new Date('10/24/' + year),
							  d = moment(fixedDate),
							  dayOfWeek = d.day();// O based dayOfWeek

						// Is it Thursday or before?
						if(dayOfWeek <= 4){
							resolve(dayOfWeekFromFixed({dayOfWeek:'Sun',direction:'before',fixedDate}));
						}else{
							resolve(dayOfWeekFromFixed({dayOfWeek:'Sun',direction:'after',fixedDate}));
						}
					}),

					/**
					 * protectionTheotokos
					 */
					protectionTheotokos : singleFixed({date:'10/14/' + year}),

					/**
					 * Saturday before 11/08 NC
					 */
					demetriusSaturday : new Promise(function(resolve,reject) {
						resolve(dayOfWeekFromFixed({dayOfWeek:'Sat',direction:'before',fixedDate:new Date('11/08/' + year)}));
					}),

					/**
					 * forefeastPresentationOfTheotokos
					 */
					forefeastPresentationOfTheotokos : singleFixed({date:'12/3/' + year}),

					/**
					 * presentationTheotokos
					 */
					presentationTheotokos : singleFixed({date:'12/4/' + year}),

					/**
					 * afterfeastPresentationTheotokos
					 */
					afterfeastPresentationTheotokos : rangeFixed({start:'12/5/' + year,end:'12/8/' + year}),

					/**
					 * apodosisPresentationTheotokos
					 */
					apodosisPresentationTheotokos : singleFixed({date:'12/8/' + year})
				};

				//Run async over above object then return request to caller
				return Promise.props(periods);

			}).then(function (obj) {
			resolve(obj);
		});

	});
};




/**
 * Calculates moveable feast single date value
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
function singleMoveable(args){
	return new Promise(function(resolve,reject) {
		var p = new Date(args.pascha);

		// Polarity controls the +- operation
		if(args.polarity){
			p.setDate(p.getDate() + args.offset);
		}else{
			p.setDate(p.getDate() - args.offset);
		}

		resolve(p);
	});
}

/**
 * Returns a single fixed date
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
function singleFixed(args){
	return new Promise(function(resolve,reject) {
		resolve(new Date(args.date));
	});
}

/**
 * Calculates moveable feast range values
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
function rangeMoveable(args){
	return new Promise(function(resolve,reject) {
		var s = new Date(args.pascha),
			e = new Date(args.pascha),
			range = {};

		// Polarity controls the +- operation
		if(args.offset.polarity){
			s.setDate(s.getDate() + args.offset.start);
			e.setDate(e.getDate() + args.offset.end);
		}else{
			s.setDate(s.getDate() - args.offset.start);
			e.setDate(e.getDate() - args.offset.end);
		}

		range.start = s;
		range.end = e;

		resolve(range);
	});
}

/**
 * Retuns a range fixed date
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
function rangeFixed(args){
	return new Promise(function(resolve,reject) {
		var range = {};
		range.start = new Date(args.start);
		range.end = new Date(args.end);

		resolve(range);
	});
}

/**
 * Calculates the date of the day of week before or after a fixed date
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
function dayOfWeekFromFixed(args){
	const {fixedDate,dayOfWeek,direction} = args,//direction == 'before' or 'after'
		  inputDayOfWeek = moment(fixedDate).weekday(),
		  targetDayOfWeek = utils.convertStringToDayNum(dayOfWeek),
		  sunday = moment(fixedDate).startOf('week').toDate();

	let d = new Date(fixedDate);

	if(direction === 'before' && inputDayOfWeek === targetDayOfWeek){

		return moment(sunday).subtract(7,'days').toDate();
	}else if(direction === 'before'){

		if(targetDayOfWeek < inputDayOfWeek){

			return moment(sunday).add(targetDayOfWeek,'days').toDate();
		}else if(targetDayOfWeek > inputDayOfWeek){

			//go back a week, then find our day number
			return moment(fixedDate).subtract(1, 'weeks').isoWeekday(targetDayOfWeek).toDate();
		}
	}else if(direction === 'after' && inputDayOfWeek === targetDayOfWeek){

		return moment(sunday).add(7,'days').toDate();
	}else if(direction === 'after'){
		if(inputDayOfWeek < targetDayOfWeek){

			//add the difference to sunday
			return moment(sunday).add((inputDayOfWeek - targetDayOfWeek),'days').toDate()
		}else if(inputDayOfWeek > targetDayOfWeek){

			//JUmp into next week and grab the date by day of the week
			return moment(fixedDate).add(1, 'weeks').isoWeekday(targetDayOfWeek).toDate();
		}
	}

}

/**
 * Calcs nativity for next year
 * @param year
 * @returns {{}}
 */
function getNativityFastForFollowingYear(yr){
	var followingYear = typeof yr === "number" ? yr + 1 : parseInt(yr,10)+1,
		s = new Date('1/7/' + followingYear),
		e = new Date('1/7/' + followingYear),
		range = {};

	//-40 days before Nativity
	s.setDate(s.getDate() - 40);
	//Last day is day before Nativity
	e.setDate(e.getDate() - 1);
	range.start = s;
	range.end = e;

	return range;
}

