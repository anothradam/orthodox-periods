'use strict'
import _ from 'underscore';
import {Calendar as Cal} from 'calendar';
import dates from './Dates';
import fasts from './Fasts';
import Periods from './Periods';
import greatLent from './GreatLent';
import color from './Color';
import tones from './Tones';
import weeklycycle from './WeeklyCycle';
import gospels from './Gospels';
import epistles from './Epistles';
import psalter from './Psalter'; 


var moment  = require('moment'),
    Promise = require('bluebird'),
	utils = require('./utils'),
	variablePortions = require('./variablePortions.json'),
	exapostilaria = require('./resurrectionExapostilaria.json'),
	l = console.log,

Calendar = {

	/**
	 * Makes a calendar collection - to be used as a base for the makeWithData method
	 *
	 * @param args
	 * @returns {bluebird|exports|module.exports}
	 */
	getForYear: args=>{
		let cMon = new Cal(0),
			calendarYear = []; // weeks starts on Sunday

		// Build each month
		for(let a=0; a<12;a++){
			calendarYear = calendarYear.concat(makeMonth(args.year,a));
		}

		// Makes a month array and all dates
		function makeMonth(year,month){
			let mdc = cMon.monthDays(year, month),// 0 = January
				tmp_month = [];

			// Loop through the weeks then through the days in the week to build an object
			for (let i=0; i<mdc.length; i++){
				mdc[i].forEach((date,j)=>{
					if(date>0){
						let o = {};
							o.month = month+1;
							o.monthLabel = utils.convertMonthNumToString(month+1);// 1 = January
							o.dayOfMonth = date;
							o.dayOfWeek = j;
							o.dayOfWeekLabel = utils.convertDayNumToString(j);
							o.date = new Date(`${month+1}/${date}/${year}`);

						tmp_month.push(o);

					}
				});
			}

			return tmp_month;
		}

		return calendarYear;
	},


	/**
	 * Make the year's calendar with all the church data
	 * @param args
	 */
	makeWithData: args=>new Promise((resolve, reject)=> {
		let calendar = Calendar.getForYear(args);

		dates({year:args.year,calendar:args.calendar})
			.then(periods=>{
				let i = 0;

				// Recurse over each calendar day so we can use promises and return when ready
				(function recurseCalendar(){
					let currentDate = calendar[i];

					if(i<calendar.length){

						// Promise items that require the current date to calculate
						Promise.all([
							fasts.isFastDay({date:currentDate.date,calendar:args.calendar}),
							Periods.getWeekAfterPentecost({date:currentDate.date,calendar:args.calendar}),
							Periods.getWeekAfterPascha({date:currentDate.date,calendar:args.calendar}),
							greatLent.is({date:currentDate.date,calendar:args.calendar}),
							greatLent.getWeekOf({date:currentDate.date,calendar:args.calendar}),
							color.getByDate({date:currentDate.date,calendar:args.calendar}),
							weeklycycle.getByDate({date:currentDate.date,calendar:args.calendar}),
							gospels.getByDate({date:currentDate.date,calendar:args.calendar}),
							epistles.getByDate({date:currentDate.date,calendar:args.calendar}),
							tones.getByDate({date:currentDate.date,calendar:args.calendar}),
							psalter.getKathismaNumbersByDate({date:currentDate.date,calendar:args.calendar}),
							gospels.getSundayMatinsGospel({date:currentDate.date,calendar:args.calendar}),
							fasts.isFastFreePeriod({date:currentDate.date,calendar:args.calendar})
							//@todo Epistle make a get by date function & Fix nativity fast calculation because of cross
							// year calculation problems
							// Fix the Sunday matins gospel calculation It's wrong. See january 3rd 2016
						]).then(res=>{
							// Create the base calendar with periods built. Append promise results after this base
							currentDate = Object.assign({},currentDate,makeDatePeriods(currentDate));
							currentDate.fastingRules = res[0];
							currentDate.weekAfterPentecost = res[1];
							currentDate.weekAfterPascha = res[2];
							currentDate.isGreatLent = res[3];
							currentDate.weekOfGreatLent = res[4];
							currentDate.color = res[5];
							currentDate.weeklycycle = res[6];
							currentDate.gospels = res[7];
							currentDate.epistles = res[8];
							currentDate.tone = res[9].tone;
							currentDate.psalter = res[10];
							currentDate.sundayMatinsGospel = currentDate.dayOfWeek == 0 ? res[11] : false;//service.split(' ')[1]<-- eothina#

							// push fast Free into periods if applicable
							if(res[12]){ currentDate.periods.push('fastFree'); }

							// Based on sunday matins resurrection gospel number 1-11
							const eothina = res[10][0] ? res[10][0].service.split(' ')[1] : false;
							currentDate.exapostilaria = currentDate.dayOfWeek == 0 ? _.findWhere(exapostilaria,{number:parseInt(eothina,10)}) : false;

							// Based on tone and dayOfWeek
							const variables = _.findWhere(variablePortions,{dayOfWeek:String(currentDate.dayOfWeek),tone:String(res[8].tone)});
							currentDate.variablePortions = variables ? variables : false;

							calendar[i] = currentDate;
							i++;
							recurseCalendar();
						});
					}else{
						//console.log('cal',calendar);
						resolve(calendar);
					}
				})();

				// Make date periods on calendar
				function makeDatePeriods(dt){
					var date = Object.assign({},dt),
						momentDate = moment(new Date(date.date)),
						datePeriods = [];

					// FOr each date loop through each period to compare dates to see if
					// we're within a period range or on a feast
					for(let key in periods){
						if(utils.dateWithinPeriodRange({periods,momentDate},key,key)){
							datePeriods.push(key);
						}
					}
					date.periods = datePeriods;

					return date;
				}
			});
	})

};
/**
 * Export
 * @type {{}}
 */
export default Calendar;