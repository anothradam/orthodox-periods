var Promise = require('bluebird'),
	dates = require('./Dates'),
	utils 	= require('./utils'),
	Forefeast = {};

/**
 * Returns all forefeasts for given periods collection
 *
 * orthodoxDates
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Forefeast.getAllForYear = args=>new Promise(function(resolve,reject){
	const {calendar,year} = args;
	let key,
		forefeasts 	= {};

	// Get all periods for request date
	dates({year,calendar})
		.then(orthodoxDates=> {
			for (key in orthodoxDates) {
				if (key == 'forefeastAscension') { forefeasts['forefeastAscension'] = orthodoxDates[key];}
				if (key == 'forefeastNativity') { forefeasts['forefeastNativity'] = orthodoxDates[key];}
				if (key == 'forefeastTheophany') { forefeasts['forefeastTheophany'] = orthodoxDates[key];}
				if (key == 'forefeastPresentationChrist') { forefeasts['forefeastPresentationChrist'] = orthodoxDates[key];}
				if (key == 'forefeastAnnunciation') { forefeasts['forefeastAnnunciation'] = orthodoxDates[key];}
				if (key == 'forefeastTransfiguration') { forefeasts['forefeastTransfiguration'] = orthodoxDates[key];}
				if (key == 'forefeastDormition') { forefeasts['forefeastDormition'] = orthodoxDates[key];}
				if (key == 'forefeastNativityTheotokos') { forefeasts['forefeastNativityTheotokos'] = orthodoxDates[key];}
				if (key == 'forefeastExaltationCross') { forefeasts['forefeastExaltationCross'] = orthodoxDates[key];}
				if (key == 'forefeastPresentationTheotokos') {forefeasts['forefeastPresentationTheotokos'] = orthodoxDates[key];}
			}

			resolve(forefeasts);
		});
});

/**
 * Export
 * @type {{}}
 */
export default Forefeast;