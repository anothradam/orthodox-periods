var Promise = require('bluebird'),
    moment = require('moment'),
	utils   = require('./utils.js'),
	dates 	= require('./Dates'),
    Tones   = {};


/**
 * Gets the tone of any week passed to the function
 *
 * @todo The tones are entirely incorrect at the moment
 *
 * args.weekOfYear
 * args.periods
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Tones.getByDate = args => new Promise((resolve,reject) => {
    const {calendar} = args;
    let weeksRelativeToTone1;

	args = utils.dateObj(args);

    //Get tone 1 date
    dates({year:args.year,calendar})
        .then(thisYearDates=>{
            const thomasSunday      	    = thisYearDates.thomasSunday,
                  dateIsBeforeThomasSunday  = moment(args.momentDate).isBefore(thomasSunday),
                  dateIsAfterThomasSunday 	= moment(args.momentDate).isAfter(thomasSunday);

            if(dateIsBeforeThomasSunday){

                // Get previous year dates
                dates({year:(args.year-1),calendar})
                    .then(previousYearDates=>{

                        // Tones cease for the year at palm Sunday
                        if(moment(args.momentDate).isBefore(thisYearDates.palmSunday)){

                            // GEt total number of weeks in previous year
                            // Calculates week from the next year. Year - previous year thomas Sunday num + current year week num
                            weeksRelativeToTone1 = (utils.dateObj(`${args.month}-${args.dayOfMonth}-${args.year-1}`).totalWeeksInYear - utils.dateObj(previousYearDates.thomasSunday).weekOfYear) + args.weekOfYear-1;

                            resolve({tone : getToneByWeek(weeksRelativeToTone1)});
                        }else{
                            resolve({tone:false});
                        }
                    });

            }else if(dateIsAfterThomasSunday){
                // Get following year dates
                dates({year:(args.year+1),calendar})
                    .then(followingYearDates=>{

                        // Tones cease for the year at palm Sunday
                        if(moment(args.momentDate).isBefore(followingYearDates.palmSunday)){

                            // current week num - thomasSunday week num = weeks after thomasSunday
                            weeksRelativeToTone1 = (args.weekOfYear - utils.dateObj(thisYearDates.thomasSunday).weekOfYear);

                            if(weeksRelativeToTone1 === 0){
                                resolve({tone : 1});
                            }else{
                                resolve({tone : getToneByWeek(weeksRelativeToTone1)});
                            }
                        }else{
                            resolve({tone:false});
                        }
                    });
            }else{
                resolve({tone:1});//thomas sunday
            }





            ////Add d.weekOfYear if it's after tone1Wk and vice versa
            //if(tone1Wk > args.weekOfYear){
            //    weeksRelativeToTone1 = tone1Wk - args.weekOfYear;
            //}else if(tone1Wk < args.weekOfYear){
            //    weeksRelativeToTone1 = args.weekOfYear - tone1Wk;
            //}else{
            //    tone = 1;
			//
            //    resolve(tone+'');
            //}
			//
            ////@todo fix tones logic here
            ////Seems ok up to Novemebre?
			//
            //// Loop through 8 tones array, in multiples of 8
            //// to land on the correct index
            //for(var i=0;i<weeksRelativeToTone1;i++){
            //    if(i%8 == 0){ //length of tones array
            //        iterator = 0; //reset iterator
            //    }
            //    iterator++;
            //}
			//
            //tone = tones[(iterator%8)];
			//
            //resolve(tone+'');
        });//_tone1
});

/**
 * Utility for returning the link to Tone Tutor files on drive
 *
 * args.tone
 *
 * @param tone
 * @returns {string}
 */
Tones.getToneTutorFilesLink = function(args){
    switch(parseInt(args.tone)){
        case 1 :
            return 'https://drive.google.com/open?id=0B0zKJFuIn1HNfmRDYmE1YlVpSU1NOEpPLTBXMDkxZXdWZHZCTkJmTW5HSVA5TzRMQm1SUUU';
            break;
        case 2 :
            return 'https://drive.google.com/open?id=0B0zKJFuIn1HNfjJlNEw0TTJUYXlhOFVGXzdrUllxMHh1enBhczdvNEdHWFN5RThSMDVyR28';
            break;
        case 3 :
            return 'https://drive.google.com/open?id=0B0zKJFuIn1HNfm0wSm9mWndTTlQxQl8tLXdodnEtTGQxZm4wMDNMdDJiLUlYU3QyQllIbzQ';
            break;
        case 4 :
            return 'https://drive.google.com/open?id=0B0zKJFuIn1HNflJOdklsc1Rrd2tlaTh0SXItQXdkT25pLU1teTF2VmNCQzAtalY2TDE3TFU';
            break;
        case 5 :
            return 'https://drive.google.com/open?id=0B0zKJFuIn1HNfjUwRXFxX3Jfc0JSNDh4NXM5RV9ub01wajNOOE9IYlVSTlY0b0NCcjZsbEU';
            break;
        case 6 :
            return 'https://drive.google.com/open?id=0B0zKJFuIn1HNfmFFdHJUWlBIcEkzWWpuWENtRnV2SDBJR1paWFdwNThvR2NnNmc0ejM4eWc';
            break;
        case 7 :
            return 'https://drive.google.com/open?id=0B0zKJFuIn1HNfmRNR2pncDlLRzVuUkJMUmZuZHZrQmVMUTVwZDYxeE15YVdDZXZVZ0VhTWc';
            break;
        case 8 :
            return 'https://drive.google.com/open?id=0B0zKJFuIn1HNfkVRWTNPTDd6bmpfRThWNEozTzZFcnNYRUlwYW05ZVhwU2lKM29RX1pWZTA';
            break;
    }
};


/**
 * Utility func to match the week number to the correct tone according to a multiple of 8 cycle
 * @param weeksRelativeToTone1
 * @returns {number}
 */
function getToneByWeek(weeksRelativeToTone1){
    const tones = [1,2,3,4,5,6,7,8];
    let iterator = 1;

    // Loop through 8 tones array, in multiples of 8
    // to land on the correct index
    for(var i=0;i<weeksRelativeToTone1;i++){
        if(i%8 == 0){ //length of tones array
            iterator = 0; //reset iterator
        }
        iterator++;
    }

    return tones[(iterator%8)];
}



/**
 * Export
 * @type {Tones}
 */
export default Tones;