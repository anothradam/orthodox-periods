'use strict';
import _ from 'underscore';
import Dates from './Dates';
import Periods from './Periods';
import GreatLent from './GreatLent';
import utils from './utils';

var moment  = require('moment'),
    Promise = require('bluebird'),
	gospelsCollection = require('./gospels.json'),// DayOfWeek is Sunday = 0
	l = console.log;
	require('moment-range');

const Gospels = { 

	/**
	 * Retrieves a date's gospel reading
	 *
	 * @param args
	 */
	getByDate : args => new Promise((resolve,reject) => {
		const {calendar} = args;
		args = utils.dateObj(args);
		args.calendar = calendar;

		/**
		 * 1. Checks for a match on exact date
		 * 2. Check for weekAfterPentecost in args
		 * 3. Check for weekAfterPascha in args
		 */
		Promise.all([
			Gospels.getByFixedDate(args),
			Periods.getWeekAfterPentecost({date:args.date,calendar:args.calendar}),
			Periods.getWeekAfterPascha({date:args.date,calendar:args.calendar}),
			Dates({year:args.year,calendar:args.calendar}),
			GreatLent.is({date:args.date,calendar:args.calendar}),
			Gospels.isInLukanJump(args)
		])
		.then(res=> {
			let resultArray = [],
				fixedDateReading = res[0];
			args.weekAfterPentecost = res[1].week;
			args.weekAfterPascha = res[2].week;
			args.periods = res[3];
			args.isGreatLent = res[4];
			args.isInLukanJump = res[5];

			/**
			 * Get by fixed date
			 * @todo commented out because there are too many incorrect results returned
			 */
			//if (fixedDateReading.length > 0) {
			//	resultArray = resultArray.concat(_makeReadings(fixedDateReading));
			//}

			// HAndle fetching lukan Jump readings
			if(args.isInLukanJump){
				Gospels.getLukanJumpReadingByDate(args)
					.then(lukanReading=>{
						if(lukanReading.length){
							resultArray = resultArray.concat(_makeReadings(lukanReading));

							resolve(resultArray);
						}else{
							// This shouldn't be necessary, if We're in Lukan Jump it needs to be returned
							resolve(resultArray);
						}
					});
			}else{
				/**
				 * Get by weekAfterPentecost, then weekAfterPascha, then default
				 */
				if (args.weekAfterPentecost){
					return Gospels.getByDayAndWeekAfterPentecost(args)
						.then(result=>{
							if (result.length) {

								resultArray = resultArray.concat(_makeReadings(result));
								/**
								 * Note: May not need the getCurrentCycleByDate function logic. Keep testing
								 */
								// Only include results from the current Gospel book cycle
								//result.forEach(item=>{
								//args.dayOfWeek = parseInt(args.dayOfWeek,10) + 1;
								//if(_getCurrentCycleOfBookByDate(args) === item.cycleOfBook){
								//	resultArray = resultArray.concat(_makeReadings([item]));
								//}
								//});
							}

							resolve(resultArray);

						}, console.log)
				}else if (args.weekAfterPascha) {
					return Gospels.getByDayAndWeekAfterPascha(args)
						.then(function (reading) {
							if (reading.length) {
								resultArray = resultArray.concat(_makeReadings(reading));
							}

							resolve(resultArray);
						}, console.log);
				}else{
					resolve(resultArray);
				}
			}
		});
	}),

	/**
	 *
	 * args.weekAfterPentecost
	 *
	 * Retrieves the Sunday Matins (Resurrection Gospel)
	 * https://en.wikipedia.org/wiki/Matins_Gospel
	 */
	getSundayMatinsGospel : args => new Promise((resolve,reject) => {
		let resultArray = [];

		return Periods.getWeekAfterPentecost({date:args.date,calendar:args.calendar})
			.then(res=>{
				args.weekAfterPentecost = res.week;

				const eothina = _determineEothina(args);
				// @todo  ADD logic for when the cycle stops and starts- incomplete ie. get By week after Pascha - figure this out
				// Fetch determined reading
				return Gospels.getByEothina({service: 'eothina '+eothina});
			})
			.then(function (reading) {

			if (reading.length > 0) {
				resultArray = resultArray.concat(_makeReadings(reading));

				resolve(resultArray);
			} else {
				resolve(false);
			}
		}, console.log);
	}),

	/**
	 * Returns all fasting periods from the year's periods
	 *
	 * args.periods
	 *
	 * @param args
	 * @returns {bluebird|exports|module.exports}
	 */
	getAll: args=>new Promise((resolve, reject)=> {
		resolve(gospelsCollection);
	}),//getGospels

	/**
	 * Get epistle by week after pentecost and day requested
	 *
	 * args.weekAfterPentecost
	 * args.dayOfWeek
	 *
	 * @param args
	 * @returns {bluebird|exports|module.exports}
	 */
	getByDayAndWeekAfterPentecost : args=>new Promise(function (resolve, reject) {
		resolve(_.filter(gospelsCollection,item=>{
			if(args.dayOfWeek == 0 && item.dayOfWeek === 0){
				return (item.weekAfterPentecost-1) === args.weekAfterPentecost;
			}
			return item.weekAfterPentecost === args.weekAfterPentecost && item.dayOfWeek === args.dayOfWeek;
		}));
	}),

	/**
	 * Gets reading by day and week after Pascha
	 *
	 * args.dayOfWeek
	 * args.weekAfterPascha
	 *
	 * @param args
	 * @returns {bluebird|exports|module.exports}
	 */
	getByDayAndWeekAfterPascha : args=>new Promise(function (resolve, reject) {
		resolve(_.filter(gospelsCollection,item=>{
			return item.weekAfterPascha == args.weekAfterPascha && item.dayOfWeek == args.dayOfWeek;
		}));
	}),

	/**
	 * Get's a document by fixed date match
	 *
	 * args.month
	 * args.dayOfMonth
	 *
	 * @param args
	 * @returns {bluebird|exports|module.exports}
	 */
	getByFixedDate : args=>new Promise(function (resolve, reject) {
		resolve(_.filter(gospelsCollection,item=>{
			return item.fixedDateMonth == args.month && item.fixedDateDay == args.dayOfMonth;
		}));
	}),

	/**
	 * Gets the Resurrection Gospel (eothina) requested
	 *
	 * @param args
	 * @returns {bluebird|exports|module.exports}
	 */
	getByEothina : args=>new Promise(function (resolve, reject) {
		resolve(_.filter(gospelsCollection,item=>{
			return item.service == args.service;
		}));
	}),

	/**
	 * Return results by section number
	 * @param args
	 */
	getBySection : args=>new Promise(function (resolve, reject) {
		resolve(_.filter(gospelsCollection,item=>{
			return item.section == args.section;
		}));
	}),

	/**
	 * Determines if we're in the Lukan Jump
	 * @param args
	 */
	isInLukanJump : args=>new Promise(function (resolve, reject) {
		const {calendar} = args;
		args = utils.dateObj(args);
		args.calendar = calendar;

		//if its the first half of a year (before the exaltation),
		// then run the calculation for the previous year as well because Lukan jump overlaps the new year
		Dates({year:args.year,calendar:args.calendar})
			.then(dates=>{
				// Determine if we need to check the end of last year's Lukan jump
				if(args.momentDate.isBefore(moment(dates.exaltationCross))){

					// Get the dates from the year before
					Dates({year:args.year-1,calendar:args.calendar})
						.then(previousYearDates=>{
							resolve(args.momentDate.isBefore(moment(previousYearDates.endLukanJump)));
						});
				}else{// input date is after the exaltaion
					const start = moment(dates.beginLukanJump),
						  end = moment(dates.endLukanJump),
						  range = moment().range(start,end);

					resolve(range.contains(args.momentDate));
				}
			},console.log);
	}),

	/**
	 * Determines what week of the Lukan Jump we are in
	 * http://frjohnpeck.com/what-is-the-lukan-jump/
	 * http://www.byzcath.org/forums/ubbthreads.php/topics/133271/Lukan%20Jump
	 * @param args
	 */
	weekOfLukanJump : args=>new Promise(function (resolve, reject) {
		const {calendar} = args;
		args = utils.dateObj(args);
		args.calendar = calendar;

		Dates({year:args.year,calendar:args.calendar})
			.then(dates=>{

				// Determine if we need to check the end of last year's Lukan jump
				if(args.momentDate.isBefore(moment(dates.exaltationCross))){

					// Get the dates from the year before
					Dates({year:args.year-1,calendar:args.calendar})
						.then(previousYearDates=>{
							const pStart = moment(previousYearDates.beginLukanJump),
								  pEnd = moment(previousYearDates.endLukanJump),
								  pRange = moment().range(pStart,pEnd);

							if(pRange.contains(args.momentDate) && !pEnd.isSame(args.momentDate)){
								// +1 because moment registers same week as zero. We want to reference week 0 as the first week
								if(args.dayOfWeek === 0){//Handle starting week on Sunday
									resolve({weekOfLukanJump:moment(args.date).diff(pStart, 'week')+1});
								}else{
									resolve({weekOfLukanJump:moment(args.date).diff(pStart, 'week')});
								}
							}else{
								resolve({weekOfLukanJump:false});
							}
						});
				}else{// input date is after the exaltaion

					const start = moment(dates.beginLukanJump),
						  end = moment(dates.endLukanJump),
						  range = moment().range(start,end);

					if(range.contains(args.momentDate)){

						// +1 because moment registers same week as zero. We want to reference week 0 as the first week
						if(args.dayOfWeek === 0){//Handle starting week on Sunday
							resolve({weekOfLukanJump:moment(args.date).diff(start, 'week')+2});
						}else{
							resolve({weekOfLukanJump:moment(args.date).diff(start, 'week')+1});
						}
					}else{
						resolve({weekOfLukanJump:false});
					}
				}
			},console.log);
	}),

	/**
	 * Calculates what reading is required for a date during the Lukan Jump period.
	 *
	 * Week1 day 1 of Lukan Jump begins with the MOnday reading for the 18th week after pentecost
	 *
	 * "divided over nineteen weeks beginning on the Monday after the Elevation of the Holy Cross –
	 * from the thirteenth week, it is only read on Saturdays and Sundays, while St. Mark’s Gospel
	 * is read on the remaining weekdays"
	 * @param args
	 */
	getLukanJumpReadingByDate : args=>new Promise(function (resolve, reject) {
		const {calendar} = args;
		args = utils.dateObj(args);
		args.calendar = calendar;

		Gospels.weekOfLukanJump(args)
			.then(lukanJump=>{
				l(lukanJump);
				args.weekAfterPentecost = lukanJump.weekOfLukanJump + 17;

				if(args.weekAfterPentecost < 37){
					Gospels.getByDayAndWeekAfterPentecost(args)
						.then(lukanReading=>{

							// If after 13 weeks, just return luke on sat & sun and return mark on weekdays
							if(lukanJump.weekOfLukanJump >= 13){
								// luke is read on weekends and Mark on weekdays
								if(!args.isWeekday){
									//if(lukanReading[0].bookName !== 'Luke'){
									//	// Need to jump back to the beginning of Luke
									//	// if we run out of Luke weekend readings ?
									//	resolve(lukanReading);
									//
									//}else{
										resolve(lukanReading);
									//}
								}else{
									//resolves to mark - Weekday readings for Mark are in the lectionary
									resolve(lukanReading);
								}
							}else{
								resolve(lukanReading);
							}
						});
				}else{
					// @todo  check this: { weekOfLukanJump: 19 } 'DAYOF WEEK' 0 'Matthew'
					args.weekAfterPentecost = (lukanJump.weekOfLukanJump - 19) + 17;

					//Need to jump back after 37 weeks
					Gospels.getByDayAndWeekAfterPentecost(args)
						.then(lukanReading=>{

							// Because we're well over 13 weeks luke is read on weekends and Mark on weekdays
							if(!args.isWeekday){
								//if(lukanReading[0].bookName !== 'Luke'){
								//	// Need to jump back to the beginning of Luke
								//	// if we run out of Luke weekend readings ?
								//	resolve(lukanReading);
								//
								//}else{
									resolve(lukanReading);
								//}
							}else{
								//resolves to mark - Weekday readings for Mark are in the lectionary
								resolve(lukanReading);
							}

						});
				}
			});
	})
};
/**
 * Export
 * @type {{}}
 */
export default Gospels;


/**
 * Utility for building an array of readings
 * @param reading
 * @returns {Array}
 * @private
 */
function _makeReadings(reading) {
	var a = [];
	for (var i = 0; i < reading.length; i++) {
		a.push({
			reading: reading[i].passage,
			section: reading[i].section,
			link: utils.makeScriptureLink(encodeURI(reading[i].passage)),
			service : reading[i].service || false
		});
	}
	return a;
}

/**
 * Builds eothina on week after Pentecost
 * @param args
 * @returns {*}
 * @private
 */
function _determineEothina(args) {
	var eothina;
	args.weekAfterPentecost = args.weekAfterPentecost - 1;

	// 11 week cycle for resurrection Gospels
	if((args.weekAfterPentecost+'').length === 1){
		return args.weekAfterPentecost;// up until 12 the eothina are in order
	}else{
		switch((args.weekAfterPentecost+'').charAt(0)){
			case '1' :
				if(args.weekAfterPentecost < 12){
					return args.weekAfterPentecost;// up until 12 the eothina are in order
				}else{
					return args.weekAfterPentecost - 11;// this takes us up to week 19 eothina 8
				}
				break;
			case '2' :
				if(args.weekAfterPentecost < 23){
					return args.weekAfterPentecost - 11;// 22-11 = eothina 11
				}else{
					return args.weekAfterPentecost - 22;// this takes us up to week 29 eothina 7
				}
				break;
			case '3' :
				if(args.weekAfterPentecost < 34){
					return args.weekAfterPentecost - 22;// 33-22 = eothina 11
				}else{
					return args.weekAfterPentecost - 33;// this takes us up to week 39 eothina 6
				}
				break;
		}
	}

	return eothina;
}

/**
 * Determines what cycle of the Gospel we should be using. Based around the Lukian jump.
 * The args must be a date object including the moment date
 * LK,JN,MK,MT
 * @param args
 * @returns {string}
 * @private
 */
function _getCurrentCycleOfBookByDate(args){

	// JOHN - Between pascha and pentecost
	if(utils.dateWithinPeriodRange(args,'pascha','pentecost')){
		l('JOHN');
		return 'JN';
	}

	// MARK - Saturday or Sunday in Great Lent
	if(args.isGreatLent && (args.dayOfWeek == '6' || args.dayOfWeek == '7')){
		l('MARK');
		return 'MK';
	}

	// MATTHEW - After Monday of the Holy spirit, read for 17 weeks. After 12 weeks read only saturday and sunday and Mark is read instead
	//mondayOfHolySpirit
	let mhs17 = new Date(args.periods.mondayOfHolySpirit);
		mhs17.setDate(mhs17.getDate() + 119);//17 weeks after mondayofHolySpirit

	let mhs12 = new Date(args.periods.mondayOfHolySpirit);
		mhs12.setDate(mhs12.getDate() + 84);//12 weeks after mondayofHolySpirit

	if(args.momentDate.isAfter(moment(args.periods.mondayOfHolySpirit)) && args.momentDate.isBefore(moment(mhs17))){
		l('MATTHEW',args.dayOfWeek);
		if(args.momentDate.isAfter(moment(mhs12)) && (args.dayOfWeek == '6' || args.dayOfWeek == '7')){
			return 'MT';
		}else{
			return 'MK';
		}
	}

	// LUKE - 19 weeks after the Sunday of the Exaltation of the Cross. After the 13th week it is read only on Saturday & Sunday & Mark is read on the weekdays
	let ec19 = new Date(args.periods.exaltationCross);
	ec19.setDate(ec19.getDate() + 133);//19 weeks after exaltationCross

	let ec13 = new Date(args.periods.exaltationCross);
	ec13.setDate(ec13.getDate() + 91);//13 weeks after exaltationCross

	if(args.momentDate.isAfter(moment(args.periods.exaltationCross)) && args.momentDate.isBefore(moment(ec19))){
l('LUKE');
		if(args.momentDate.isAfter(moment(ec13)) && (args.dayOfWeek == '6' || args.dayOfWeek == '7')){
			return 'LK';
		}else{
			return 'MK';
		}
	}
}

//function chartEothina(weekAfterPentecost){
//	switch(weekAfterPentecost){
//		case 1:
//			return 1;
//			break;
//	}
//}