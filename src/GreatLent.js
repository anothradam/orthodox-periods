let Promise = require('bluebird'),
	moment 	= require('moment'),
	dates = require('./Dates'),
	utils 	= require('./utils'),
	GreatLent = {};

/**
 * Check to see if date is great Lent
 * @param date
 * @param cb
 */
GreatLent.is = args=>new Promise((resolve,reject)=>{
	const params = Object.assign({},args,utils.dateObj(args));

	// Get all periods for request date
	dates(params)
		.then(orthodoxDates=>{
			args.periods = orthodoxDates;

			resolve(utils.dateWithinPeriodRange(args,'greatLent','greatLent'));
		});
});


/**
 * Determine exact week of Great Lent
 *
 * args.momentDate
 * args.periods
 * args.isSunday
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
GreatLent.getWeekOf = args=>new Promise((resolve,reject)=>{
	const {calendar} = args;
	args = utils.dateObj(args);

	// Get all periods for request date
	dates({year:args.year,calendar})
		.then(orthodoxDates=>{
			args.periods = orthodoxDates;

			// @todo ensure the calculation for the last day of Great Lent is actually working. The Util dateWithin funciton may be wrong
			if(utils.dateWithinPeriodRange(args,'greatLent','greatLent')){

				// determine how many days after start we are then divide by 7 to get week num
				var gStart 	= moment(new Date(args.periods.greatLent.start));
				var rDate 	= moment(args.momentDate);

				// Add day to compensate for week
				var diff 	= rDate.diff(gStart, 'days',true) + 1;
				var result 	= Math.ceil(diff / 7);

				// If the 7th day of the week(Sunday), consider it the next week.
				if(args.isSunday){ result++; }

				resolve(result);

			}else{
				// @todo maybe return the week of the 3 week preLenten season if it's true
				resolve(false);
			}
		});
});



/**
 * Get Sunday Feasts days : ie. St Mary of Egypt in Lent
 * @param weekNum
 * @param date
 * @param cb
 */
GreatLent.getSundayFeast = function(weekNum,date,cb){
    var d = new Date(date);
    var year = d.getFullYear();

	// @todo  finish Lenten Sunday logic
    // Get the range of dates for Great Lent of specified year
    // determine week by running getWeekOf
    // Using week num, run through map of feasts for Great Lent
};


var greatLentDates = {
    sun_1 : 'triumphOrthodoxy',
    sun_2 : 'stGregoryPalamas',
    sun_3 : 'venerationOfCross',
    sun_4 : 'stJohnClimacus',
    sun_5 : 'stMaryEgypt',
    sat_5 : 'saturdayAkathist',
    sat_6 : 'lazarusSaturday',
    sun_7 : 'palmSunday',
    mon_7 : '',
    tues_7 : '',
    wed_7 : '',
    thurs_7 : '',
    fri_7 : '',
    sat_7 : ''
};

/**
 * Export
 * @type {{}}
 */
export default GreatLent;
