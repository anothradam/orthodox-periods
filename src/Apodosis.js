var Promise = require('bluebird'),
	dates = require('./Dates'),
	Apodosis ={};

/**
 * Returns all apodosis
 *
 * orthodoxDates
 *
 * @param args
 * @returns {bluebird|exports|module.exports}
 */
Apodosis.getAllForYear = args=>new Promise((resolve,reject)=>{
	const {year,calendar} = args;
		let key,
			apodosis = {};

	// Get all periods for request date
	dates({year,calendar})
		.then(orthodoxDates=>{

			for(key in orthodoxDates){
				if(key == 'apodosisPascha'){				apodosis['apodosisPascha']					= orthodoxDates[key];}
				if(key == 'apodosisPentecost'){				apodosis['apodosisPentecost']				= orthodoxDates[key];}
				if(key == 'apodosisAscension'){				apodosis['apodosisAscension']				= orthodoxDates[key];}
				if(key == 'apodosisNativity'){				apodosis['apodosisNativity']				= orthodoxDates[key];}
				if(key == 'apodosisTheophany'){				apodosis['apodosisTheophany']				= orthodoxDates[key];}
				if(key == 'apodosisPresentationChrist'){	apodosis['apodosisPresentationChrist']		= orthodoxDates[key];}
				if(key == 'apodosisTransfiguration'){		apodosis['apodosisTransfiguration']			= orthodoxDates[key];}
				if(key == 'apodosisDormition'){				apodosis['apodosisDormition']				= orthodoxDates[key];}
				if(key == 'apodosisNativityTheotokos'){		apodosis['apodosisNativityTheotokos']		= orthodoxDates[key];}
				if(key == 'apodosisExaltationCross'){		apodosis['apodosisExaltationCross']			= orthodoxDates[key];}
				if(key == 'apodosisPresentationTheotokos'){	apodosis['apodosisPresentationTheotokos']	= orthodoxDates[key];}
			}

		resolve(apodosis);
	});
});


/**
 * Export
 * @type {{}}
 */
export default Apodosis;