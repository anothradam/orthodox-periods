//http://momentjs.com/docs/
const moment  = require('moment'),
	Promise = require('bluebird'),
	dates 	= require('./Dates'),
	utils 	= require('./utils'),
	Periods = {},
	  l = console.log;


/**
* get from dates
*
* @param req
* @param callback | function
*/
Periods.getPeriodsForDate = args=>new Promise(function(resolve,reject) {
	const calendar = args.calendar;
	args = utils.dateObj(args);
	var matchingPeriods = [];

	// Get all periods for request date
	dates({year:args.year,calendar})
		.then(orthodoxDates=>{

		//Run through periods objects
		for(var key in orthodoxDates) {
			var periodDate = moment(new Date(orthodoxDates[key]));

			//Check in range
			if (orthodoxDates[key].hasOwnProperty('start')) {
				var periodStart = moment(new Date(orthodoxDates[key].start));
				var periodEnd = moment(new Date(orthodoxDates[key].end));

				//console.log('OD',key,orthodoxDates[key]);

				if(key === 'nativityFast'){// Nativity fast pops up at 2 separte times per year, the beginning and end
					const followingPeriodStart = moment(new Date(orthodoxDates[key].followingYear.start)),
						followingPeriodEnd = moment(new Date(orthodoxDates[key].followingYear.end));

					//Is our request after a period start and before its end (or same day as)
					if ( ((periodStart.isBefore(args.momentDate) || periodStart.isSame(args.momentDate)) && (periodEnd.isAfter(args.momentDate) || periodEnd.isSame(args.momentDate)))
							||
						( (followingPeriodStart.isBefore(args.momentDate) || followingPeriodStart.isSame(args.momentDate)) && (followingPeriodEnd.isAfter(args.momentDate) || followingPeriodEnd.isSame(args.momentDate) )) ){

						matchingPeriods.push(key);
					}
				}else{
					//Is our request after a period start and before its end (or same day as)
					if ((periodStart.isBefore(args.momentDate) || periodStart.isSame(args.momentDate))
						&& (periodEnd.isAfter(args.momentDate) || periodEnd.isSame(args.momentDate) )) {

						matchingPeriods.push(key);
					}
				}
			} else {// not a range, exact date
				//Is Same
				if (periodDate.isSame(args.momentDate)) {
					matchingPeriods.push(key);
				}
			}
		}

		resolve(matchingPeriods);
	});
});


/**
* Returns the Week after Pentecost
* Format the date: "12-3-2015" (December 3 2015)
*
* @param date string
* @param callback | function
*/
Periods.getWeekAfterPentecost = args=>new Promise(function(resolve,reject){
	const calendar = args.calendar;
	args = utils.dateObj(args);
	let weekAfterPentecost = false;

	dates({year:args.year,calendar})
		.then(thisYearDates=>{
			const pentecostDate      	= thisYearDates.pentecost,
				dateIsBeforePentecost 	= args.momentDate.isBefore(moment(pentecostDate)),
				dateIsAfterPentecost 	= args.momentDate.isAfter(moment(pentecostDate));

			if(dateIsBeforePentecost){

				// Get previous year dates
				dates({year:(args.year-1),calendar})
					.then(previousYearDates=>{

						if(args.momentDate.isBefore(thisYearDates.publicanPharisee)){

							// Adjust for bizarre moment week of year handling of first week
							if(args.month === 1 && args.weekOfYear >= 52){
								args.weekOfYear = 0;
							}

							// GEt total number of weeks in previous year
							// Calculates week from the next year. Year - previous year pentecost num + current year week num
							weekAfterPentecost = (utils.dateObj(`${args.month}-${args.dayOfMonth}-${args.year-1}`).totalWeeksInYear - utils.dateObj(previousYearDates.pentecost).weekOfYear) + args.weekOfYear;

							// Sunday after pentecost is minus 1 because moment starts the week at Sunday
							resolve({
								week : weekAfterPentecost,
								sundayAfterPentecost :  args.isSunday ? weekAfterPentecost - 1 : false
							});
						}else{
							resolve({week:false});
						}
					});

			}else if(dateIsAfterPentecost){

				// Get following year dates
				dates({year:(args.year+1),calendar})
					.then(followingYearDates=>{
						if(moment(args.momentDate).isBefore(followingYearDates.publicanPharisee)){

							// current week num - pentecost week num = weeks after pentecost
							// Add 1 because the first week counts as week 1
							weekAfterPentecost = (args.weekOfYear - utils.dateObj(thisYearDates.pentecost).weekOfYear);

							// Sunday after pentecost is minus 1 because moment starts the week at Sunday
							resolve({
								week : weekAfterPentecost,
								sundayAfterPentecost :  args.isSunday ? weekAfterPentecost : false
							});
						}else{
							resolve({week:false});
						}
					});
			}else{
				//same day as pentecost
				resolve({week: 1});
			}
	});
});



/**
 * Retrieve what week after Pascha this is
 * @param args
 */
Periods.getWeekAfterPascha = args=> new Promise(function(resolve,reject){
	const calendar = args.calendar;
	args = utils.dateObj(args);

	dates({year:args.year,calendar})
		.then(orthodoxDates=>{
		const pd = utils.dateObj(orthodoxDates.pascha),
			weekAfterPascha = args.weekOfYear - pd.weekOfYear,
			//pentecostDate = utils.makeGregorian(orthodoxDates.pentecost);
			pentecostDate = orthodoxDates.pentecost;

		// Return weekAfterPascha if request date is before pentecost but after pascha
		if(args.momentDate.isAfter(pd.momentDate) && args.momentDate.isBefore(moment(pentecostDate))){
			resolve({
				week : weekAfterPascha,
				sundayAfterPascha :  args.isSunday ? (weekAfterPascha+1) : false
			});
		}else{
			resolve({week:false});
		}
	});
});



/**
 * Export
 * @type {{}}
 */
export default Periods;